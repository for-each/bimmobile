//
//  RDVAppData.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/11/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RDVModel.h"
#import <FELocationManager.h>

#define appData [RDVAppData sharedInstance]

@interface RDVAppData : NSObject

+ (RDVAppData *)sharedInstance;

@property (nonatomic, strong) FELocationManager* locationManager;
@property (nonatomic) NSInteger imageSize;
@property (nonatomic) NSInteger userHeight;
@property (nonatomic) NSInteger userHeightMode;
@property (nonatomic) RDVModel* currentModel;
@property (nonatomic) BOOL      allowAutoLocationUpdates;

// Recents:
@property (nonatomic, strong) NSMutableArray* recentModels;
- (void)addRecentModel:(RDVModel*)newModel;

@end
