//
//  RDVLocationPoint.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVBaseObject.h"

@interface RDVLocationPoint : RDVBaseObject

@property (nonatomic, strong) NSNumber* X;
@property (nonatomic, strong) NSNumber* Y;
@property (nonatomic, strong) NSNumber* Z;

- (id) initWithX:(double)xPos Y:(double)yPos Z:(double)zPos;

@end
