//
//  RDVLoadingViewController.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVLoadingViewController.h"
#import "RDVModelInfoRO.h"
#import "RDVPanoView.h"
#import "RDVPanoViewController.h"
#import <SDWebImage/SDWebImageManager.h>

@interface RDVLoadingViewController ()
@property (weak, nonatomic) IBOutlet RDVPanoView *panoView;
@property (weak, nonatomic) IBOutlet UIProgressView *loadingProgress;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (nonatomic, strong) NSMutableDictionary* imagesDict;
@property (nonatomic, strong) RDVModelInfoRO* modelInfoRO;
@property (weak, nonatomic) IBOutlet UIButton *finishedLoadingButton;
@property (weak, nonatomic) IBOutlet UILabel *previewNA;

@end

@implementation RDVLoadingViewController

- (NSString *)viewControllerDisplayName
{
    return @"Loading Model View";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Setup the back button:
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem rdvBackBarButtonWithTarget:self.navigationController selector:@selector(popViewControllerAnimated:)];
    
    self.modelInfoRO = [RDVModelInfoRO new];
    self.imagesDict = [NSMutableDictionary dictionaryWithCapacity:NUMBER_OF_IMAGES];
    self.panoView.allowGyroInteraction = YES;
    
    // Don't download more images
    [[SDWebImageManager sharedManager] cancelAll];
        
    // Download all of the images
    for (NSNumber* number in [NSArray rdvImageTypesArray])
    {
        // Getting the value
        RDVImageType type = number.integerValue;
        NSURL* url = [self.modelInfo.preview_urls urlForType:type];
        
        // Download this image
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:url
                                                              options:SDWebImageDownloaderUseNSURLCache
         progress:nil
        completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
        {
            // Only if no error
            if (error == nil && image != nil)
            {
                self.imagesDict[[NSString stringWithRDVImageType:type]] = image;
                
                if (self.imagesDict.allKeys.count == NUMBER_OF_IMAGES)
                {
                    [self.panoView loadImagesWithRDVImagesDict:self.imagesDict];
                    [self loadModelInfo];
                }
            }
            else
            {
                NSLog(@"An error occured while loading image: %@ - %@", url, error.debugDescription);
                self.previewNA.hidden = NO;
                [self loadModelInfo];
            }
        }];
    }
}

- (void)loadModelInfo
{
    [self.modelInfoRO loadModelWithModelID:self.modelInfo.modelCode andCompletion:^(RDVModelInfo* serverResponse)
     {
         self.modelInfo = serverResponse;
         NSLog(@"Loading is %d", self.modelInfo.loaded_percent);
         
         CGFloat newProgress = (CGFloat)self.modelInfo.loaded_percent / 100.0f;
         self.loadingLabel.text = self.modelInfo.loading_message;

         // Only animate when getting bigger
         [self.loadingProgress setProgress:newProgress
                                  animated:newProgress > self.loadingProgress.progress];

     
         // If we are not there yet
         if ((!self.modelInfo.didFinishLoading) && (self.navigationController.topViewController == self))
         {
             [self performSelector:@selector(loadModelInfo) withObject:nil afterDelay:1.5];
         }
         // We can continue
         else
         {
             // Hide the progress bar
             self.loadingLabel.hidden = YES;
             self.loadingProgress.hidden = YES;
             
             // Show the continue button
             self.finishedLoadingButton.hidden = NO;
         }
     }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Use gyro
    [self.panoView startAccelerometerUpdates];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Use gyro
    [self.panoView stopAccelerometerUpdates];
}

#pragma mark - Events
- (IBAction)finishedLoadingPressed:(id)sender
{
    [self performSegueWithIdentifier:@"presentModelSegue" sender:self.modelInfo];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    RDVPanoViewController* panoView = segue.destinationViewController;
    
    // Setup the model
    panoView.model = [RDVModel new];
    panoView.model.modelID = self.modelInfo.modelCode;
}


- (void)viewDidUnload {
    [self setPreviewNA:nil];
    [super viewDidUnload];
}
@end
