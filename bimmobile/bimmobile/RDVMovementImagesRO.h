//
//  RDVMovementImages.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVBaseRO.h"
#import "RDVBaseObject.h"
#import "RDVLocationPoint.h"

@interface RDVMovementImagesRO : RDVBaseRO

- (void) moveInModelID:(NSString*)modelID
          fromLocation:(RDVLocationPoint*)currLocation
           inDirection:(CGPoint)vector
         andCompletion:(FEROCompletionBlock)completion;
@end

@interface RDVGPSImagesRO : RDVBaseRO

- (void) moveInModelID:(NSString*)modelID
         toGPSLocation:(CLLocation*)location
                 flags:(NSInteger)flags
         andCompletion:(FEROCompletionBlock)completion;
@end

@interface RDVMovementResponse : RDVBaseObject

@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) RDVLocationPoint* point;

@end
