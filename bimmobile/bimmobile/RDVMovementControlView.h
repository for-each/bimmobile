//
//  RDVMovementControlView.h
//  WOW
//
//  Created by Elad Lebovitch on 7/17/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    RDVMovementDirectionUP = 0,
    RDVMovementDirectionRight = 1,
    RDVMovementDirectionDown = 2,
    RDVMovementDirectionLeft = 3
}RDVMovementDirection;

@class RDVMovementControlView;
@protocol RDVMovementControlDelegate <NSObject>

- (void) movementButtonTapped:(NSInteger)direction;

@end

@interface RDVMovementControlView : UIView
@property (weak, nonatomic) id<RDVMovementControlDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;

@end
