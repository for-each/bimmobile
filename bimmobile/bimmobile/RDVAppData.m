//
//  RDVAppData.m
//  bimmobile
//
//  Created by Elad Lebovitch on 6/11/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVAppData.h"

@implementation RDVAppData

static RDVAppData* staticAppData;

NSString* const RDV_USER_DEF_RECENT_MODELS = @"RDV_USER_DEF_RECENT_MODELS";

+ (RDVAppData *)sharedInstance
{
    if (staticAppData == nil)
    {
        staticAppData = [[RDVAppData alloc] init];
    }
    
    return staticAppData;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.locationManager = [FELocationManager new];
        
        // Meters
        self.locationManager.minNumOfMetersForNotification = MIN_NUMBER_OF_METERS_FOR_NOTIF;
        
        // Seconds
        self.locationManager.locationUpdateInterval = LOCATION_UPDATE_INTERVAL;
        
        // Setting the initial image size
        self.imageSize = RDV_DEFAULT_IMAGE_QUALITY;
        
        // Set the height
        self.userHeight = USER_DEFAULT_HEIGHT;
        self.userHeightMode = USER_DEFAULT_HEIGHT_MODE;
        self.allowAutoLocationUpdates = NO;
        
        // Load recent models:
        NSData* recentData = [[NSUserDefaults standardUserDefaults] objectForKey:RDV_USER_DEF_RECENT_MODELS];
        self.recentModels = [NSKeyedUnarchiver unarchiveObjectWithData: recentData];
        
        // Location service setup
        self.locationManager.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.locationManager.headingFilter = 1;
        [self.locationManager.locationManager startUpdatingHeading];

    }
    return self;
}

- (void)addRecentModel:(RDVModel*)newModel
{
    // Make sure it's valid
    if (newModel != nil)
    {
        // Check if we have this model already
        NSArray* copiedArray = [NSArray arrayWithArray:self.recentModels];
        NSInteger objectIndex = [copiedArray indexOfObject:newModel];
        
        // Reorder the new object
        if (objectIndex != NSNotFound)
        {
            // Advance it to the begining:
            RDVModel* oldModel = self.recentModels[objectIndex];
            if (oldModel.modelImage != nil && newModel.modelImage == nil)
            {
                newModel.modelImage = oldModel.modelImage;
            }
            
            [self.recentModels removeObjectAtIndex:objectIndex];
            [self.recentModels insertObject:newModel atIndex:0];
            
        }
        // If we didn't find it
        else
        {
            // Create an array if needed
            if (self.recentModels == nil)
            {
                self.recentModels = [NSMutableArray arrayWithCapacity:5];
            }
            
            [self.recentModels insertObject:newModel atIndex:0];
        }
        
        // Get the data
        NSData* modelData = [NSKeyedArchiver archivedDataWithRootObject:self.recentModels];
        [[NSUserDefaults standardUserDefaults] setObject:modelData forKey:RDV_USER_DEF_RECENT_MODELS];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end