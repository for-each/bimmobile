//
//  General.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/18/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#ifndef bimmobile_General_h
#define bimmobile_General_h

// Image settings
typedef enum
{
    RDVImageQualityLow = 256,
    RDVImageQualityMed = 512,
    RDVImageQualityHigh = 1024
}RDVImageQuality;

typedef enum
{
    RDVImageTypeTop = 't',
    RDVImageTypeBottom = 'b',
    RDVImageTypeWest = 'w',
    RDVImageTypeNorth = 'n',
    RDVImageTypeSouth = 's',
    RDVImageTypeEast = 'e'
}RDVImageType;

#define RDV_DEFAULT_IMAGE_QUALITY RDVImageQualityMed

// Location settings
#define isiPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define MIN_NUMBER_OF_METERS_FOR_NOTIF 2
#define LOCATION_UPDATE_INTERVAL 1
#define NUMBER_OF_IMAGES 6

#define USER_DEFAULT_HEIGHT 2
#define USER_DEFAULT_HEIGHT_MODE 1 // Relative

#import "NSArray+RDVHelpers.h"
#import "JAPanoView+RDVHelpers.h"
#import "UIBarButtonItem+RDVHelpers.h"

CG_INLINE CGFloat DegreesToRadians(CGFloat degrees)
{
    return degrees * M_PI / 180;
};

CG_INLINE CGFloat RadiansToDegrees(CGFloat radians)
{
    return radians * 180 / M_PI;
};

CG_INLINE CGFloat radiansToRDVType(CGFloat radians)
{
    // Find out which dir are we looking at:
    NSInteger degrees = RadiansToDegrees(radians);
    
    degrees %= 360;
    
    if (degrees < 0)
    {
        degrees = 360 + degrees;
    }
    
    // Make sure
    if  (degrees < 45 || degrees >= 305)
    {
        return RDVImageTypeNorth;
    }
    else if (degrees >= 45 && degrees < 135)
    {
        return RDVImageTypeEast;
    }
    else if (degrees >= 135 && degrees < 225)
    {
        return RDVImageTypeSouth;
    }
    else
    {
        return RDVImageTypeWest;
    }
};

// Pano updates
extern NSString* const NOTIF_UPDATE_PANO_VIEW;

#endif

