//
//  NSString+RDVHelpers.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "NSString+RDVHelpers.h"

@implementation NSString (RDVHelpers)

+ (NSString*) stringWithRDVImageType:(RDVImageType)imageType
{
    return [NSString stringWithFormat:@"%c", imageType];
}

@end
