//
//  RDVChooseModelViewController.h
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVViewController.h"
#import "RDVModelInfoRO.h"

@interface RDVChooseModelViewController : RDVViewController

@property (weak, nonatomic) IBOutlet UITextField *modelTextField;
@property (weak, nonatomic) IBOutlet UITableView *recentModelsTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIButton *loadModelButton;
@property (weak, nonatomic) IBOutlet UILabel *noRecentModels;
@property (strong, nonatomic) RDVModelInfoRO* modelInfoRO;

- (IBAction)btnLoadModelPressed:(id)sender;
- (IBAction)backgroundTapped:(id)sender;

@end
