//
//  RDVPanoViewController.h
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVViewController.h"
#import "RDVModel.h"
#import "RDVModelInfoRO.h"
#import "RDVLocationPoint.h"
#import <CoreMotion/CoreMotion.h>
#import "RDVMovementControlView.h"
#import "RDVPanoView.h"


@interface RDVPanoViewController : RDVViewController <CLLocationManagerDelegate, RDVMovementControlDelegate>


@property (nonatomic, strong) RDVMovementControlView* movementControl;
@property (nonatomic,strong) RDVLocationPoint* locationPoint;
@property (nonatomic) CGPoint moveToVector;

@property (nonatomic, strong) RDVModel* model;
@property (weak, nonatomic) IBOutlet RDVPanoView* panoView;
@property (nonatomic, strong) RDVModelInfo* modelInfo;

@property (strong, nonatomic) NSMutableDictionary* imageDictionary;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIProgressView *loadingProgress;

@property (weak, nonatomic) IBOutlet UIButton *btnUserInteraction;
@property (nonatomic, strong) NSTimer *zoomTimer;
@property (nonatomic) NSInteger failedImages;
@property (nonatomic) BOOL nowLoading;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;

@property (weak, nonatomic) IBOutlet UIButton *infoButton;

// Share helpers
@property (nonatomic, strong) UIActionSheet* actionSheet;

// Movement view
@property (nonatomic, strong) NSArray* currImagesURLs;

// Info view
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *altitudeLabel;

// IBActions
- (IBAction)btnUserInteractionTapped:(id)sender;

// Loading methods
- (void) loadModelInformation;
- (void) updateLocation;
- (void)loadImagesForPano;
- (void) panoNeedsUpdate;
-(void)updatedGPSPosition;

@end
