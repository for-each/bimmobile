//
//  UIFont+RDVFonts.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/2/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (RDVFonts)
+ (UIFont*) robotoMediumWithSize:(CGFloat)size;
+ (UIFont*) robotoThinWithSize:(CGFloat)size;
+ (UIFont*) robotoLightWithSize:(CGFloat)size;
@end
