//
//  RDVModel.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVModel.h"


@implementation RDVModel

NSString* const kName = @"ModelName";
NSString* const kID = @"ModelID";
NSString* const kImage = @"ModelImage";

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    // Save the image to cache, not the plist:
    if (self.modelImage)
    {
        NSData* imageData = UIImageJPEGRepresentation(self.modelImage, 1.0);
        NSString* imagePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:self.modelID];
        [imageData writeToFile:imagePath atomically:YES];
    }
    
    // Saving the object
    [aCoder encodeObject:self.modelName forKey:kName];
    [aCoder encodeObject:self.modelID forKey:kID];
}

- (id)initWithCoder:(NSCoder *)aDecoder 
{
    self = [super init];
    
    if (self)
    {
        // Get the data
        self.modelName = [aDecoder decodeObjectForKey:kName];
        self.modelID = [aDecoder decodeObjectForKey:kID];
     
        // Load the image
        NSString* imagePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:self.modelID];
        self.modelImage = [UIImage imageWithContentsOfFile:imagePath];
    }
    
    return self;
}

- (BOOL)isEqual:(id)object
{
    return [self.modelID isEqualToString:[object modelID]];
}

@end
