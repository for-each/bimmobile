//
//  RDVSettingsViewController.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/11/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RDVSettingsViewDelegate <NSObject>

- (void)settingsViewDidFinishWithChangeSettings:(BOOL)didChangeSettings;

@end

@interface RDVSettingsViewController : UITableViewController

@property (nonatomic, strong) id<RDVSettingsViewDelegate> delegate;

@end
