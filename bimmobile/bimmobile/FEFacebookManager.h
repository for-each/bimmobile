//
//  FEFacebookManager.m
//  for-each
//
//  Created by Elad Lebovitch on 10/25/12.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

typedef void(^FEFBLoginCompletionBlock)(BOOL success);
extern NSString *const FBSessionStateChangedNotification;

@interface FEFacebookManager : NSObject

@property (nonatomic, readonly) FBSession*      session;
@property (nonatomic, readonly) NSString*       accessToken;
@property (readonly)            BOOL            isConnected;
@property (nonatomic, strong)   id<FBGraphUser> fbUserDetails;
@property (nonatomic, strong)   NSArray*        friendsArray;

+ (FEFacebookManager *)sharedInstance;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void)loginToFBWithCompletionHandler:(FEFBLoginCompletionBlock)completion;
- (void)populateFriendsList;
- (void)populateUserDetailsWithCompletion:(FEFBLoginCompletionBlock)completion;
- (void)requestPostPermissionsWithBlock:(FEFBLoginCompletionBlock)block;
- (void)inviteFriendsDialog;
- (void)inviteFriendDialog:(NSString *)userFacebookID;

@end

@interface NSString (FacebookAdditions)

- (NSDictionary*)parseURLParams;

@end

@interface NSURL (FacebookAdditions)

+ (NSURL*)urlForProfileImageOfUserID:(long long)userID;

@end
