//
//  UIBarButtonItem+RDVHelpers.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (RDVHelpers)

+ (UIBarButtonItem*) rdvBackBarButtonWithTarget:(id)target selector:(SEL)selector;


@end
