//
//  NSArray+RDVHelpers.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "General.h"

@interface NSArray (RDVHelpers)

+ (NSArray*) rdvImageTypesArray;
- (NSURL*)urlForType:(RDVImageType)type;
- (NSURL*) urlForRadians:(CGFloat)radians;

@end