//
//  RDVRecentModelCell.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/2/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVRecentModelCell.h"
#import "RDVCustomFontLabel.h"
#import "UIFont+RDVFonts.h"


@interface RDVRecentModelCell ()
@property (weak, nonatomic) IBOutlet RDVCustomFontLabel *modelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *modelImage;
@property (weak, nonatomic) IBOutlet UIView *modelCodeContainer;

@end

@implementation RDVRecentModelCell

- (void)setModel:(RDVModel *)model
{
    // Only if different
    if (_model != model)
    {
        _model = model;
        
        // Set the model name
        self.modelTitle.text = model.modelName;
        
        // Do'nt present an empty title
        if  (model.modelName.length == 0)
        {
            self.modelTitle.text = [NSString stringWithFormat:@"Model %@", model.modelID];
        }
        
        // Set an image if possible
        if (model.modelImage)
        {
            self.modelImage.image = model.modelImage;
        }
        else
        {
            self.modelImage.image = [UIImage imageNamed:@"home_image_placeholder.png"];
        }
        
        // Setup code box
        [self setupModelCodeContainer];
    }
}

- (void) setupModelCodeContainer
{
    [self.modelCodeContainer.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat fCurrX = 0;
    const CGFloat kLetterSpace = 2;
    
    for (NSInteger currChar = 0; currChar < self.model.modelID.length; ++currChar)
    {
        UILabel* currLabel = [[UILabel alloc] initWithFrame:CGRectMake(fCurrX, 0,
                                                                       self.modelCodeContainer.frameHeight, self.modelCodeContainer.frameHeight)];
        currLabel.font = [UIFont robotoMediumWithSize:30];
        currLabel.textAlignment = NSTextAlignmentCenter;
        currLabel.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
        currLabel.text = [self.model.modelID substringWithRange:NSMakeRange(currChar, 1)];
        
        [self.modelCodeContainer addSubview:currLabel];
        
        fCurrX += currLabel.frameWidth + kLetterSpace;
    }
    
}

@end
