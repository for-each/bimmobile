//
//  RDVModelInfoRO.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/17/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVBaseRO.h"
#import "RDVModelInfo.h"

@interface RDVModelInfoRO : RDVBaseRO

- (void) loadModelWithModelID:(NSString*)modelID andCompletion:(FEROCompletionBlock)completion loadOffline:(BOOL)loadOffline
;
- (void) loadModelWithModelID:(NSString*)modelID andCompletion:(FEROCompletionBlock)completion;

- (void) loadModelWithModelID:(NSString*)modelID InfoOnly:(BOOL)infoOnly loadOffline:(BOOL)loadOffline andCompletion:(FEROCompletionBlock)completion;

@end
