//
//  RDVModelInfoRO.m
//  bimmobile
//
//  Created by Elad Lebovitch on 6/17/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVModelInfoRO.h"

@implementation RDVModelInfoRO

static NSString* const modelInfoActionString = @"GetModelInformation";

+(void)initialize
{
    [RDVBaseRO addResponseDescriptorForPathPattern:modelInfoActionString
                                       withMapping:[RDVModelInfo jsonMapping]];
}

- (void) loadModelWithModelID:(NSString*)modelID andCompletion:(FEROCompletionBlock)completion loadOffline:(BOOL)loadOffline
{
    // Setup a responseComplete
    FEROResponseCompleteBlock responseComplete = ^(RKObjectRequestOperation *operation,
                                                   RKMappingResult *mappingResult)
    {
        // Get the user
        RDVModelInfo* modelInfo = mappingResult.firstObject;
        modelInfo.modelCode = modelID;
        
        // Call the completion block (if set)
        completion(modelInfo);
    };
    
    NSMutableDictionary* params = [@{@"ModelId": modelID} mutableCopy];
    
    // Check if offline loaded?
    if (loadOffline)
    {
        params[@"LoadOffline"] = @"true";
    }
    
    // Send data to server
    [self loadObjectWithParams:params withCompletionBlock:responseComplete];
}

- (void) loadModelWithModelID:(NSString*)modelID InfoOnly:(BOOL)infoOnly loadOffline:(BOOL)loadOffline andCompletion:(FEROCompletionBlock)completion
{
    // Setup a responseComplete
    FEROResponseCompleteBlock responseComplete = ^(RKObjectRequestOperation *operation,
                                                   RKMappingResult *mappingResult)
    {
        // Get the user
        RDVModelInfo* modelInfo = mappingResult.firstObject;
        modelInfo.modelCode = modelID;
        
        // Call the completion block (if set)
        completion(modelInfo);
    };
    
    NSMutableDictionary* params = [@{@"ModelId": modelID} mutableCopy];
    
    // Check if info only is on
    if (infoOnly)
    {
        params[@"InfoOnly"] = @"true";
    }
    
    // Check if offline loaded?
    if (loadOffline)
    {
        params[@"LoadOffline"] = @"true";
    }
    
    // Send data to server
    [self loadObjectWithParams:params withCompletionBlock:responseComplete];
}

- (void) loadModelWithModelID:(NSString*)modelID andCompletion:(FEROCompletionBlock)completion
{
    [self loadModelWithModelID:modelID andCompletion:completion loadOffline:NO];
}

- (NSString *)actionString
{
    return modelInfoActionString;
}

@end
