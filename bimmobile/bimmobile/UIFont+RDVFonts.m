//
//  UIFont+RDVFonts.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/2/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "UIFont+RDVFonts.h"

@implementation UIFont (RDVFonts)

+ (UIFont*) robotoLightWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Roboto-Light" size:size];
}

+ (UIFont*) robotoThinWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Roboto-Thin" size:size];
}

+ (UIFont*) robotoMediumWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Roboto" size:size];
}

@end
