//
//  RDVLocationPoint.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVLocationPoint.h"

@implementation RDVLocationPoint

+ (RKObjectMapping *)jsonMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[self class]];
    
    [mapping addAttributeMappingsFromArray:@[@"X",
     @"Y",
     @"Z"]];
    
    return mapping;
}

- (id) initWithX:(double)xPos Y:(double)yPos Z:(double)zPos
{
    self = [super init];
    
    if (self)
    {
        self.X = @(xPos);
        self.Y = @(yPos);
        self.Z = @(zPos);
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"X:%@, Y:%@, Z:%@", self.X, self.Y, self.Z];
}


@end
