//
//  RDVChooseModelViewController.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVChooseModelViewController.h"
#import "RDVPanoViewController.h"
#import "RDVRecentModelCell.h"
#import "RDVLoadingViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface RDVChooseModelViewController () <UITableViewDataSource, UITableViewDelegate>


@end

@implementation RDVChooseModelViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.modelInfoRO = [RDVModelInfoRO new];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Reload data
    [self.recentModelsTable reloadData];
    self.noRecentModels.hidden = appData.recentModels.count > 0;
}

- (void)viewDidUnload
{
    [self setModelTextField:nil];
    [self setRecentModelsTable:nil];
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure this is the segue
    if ([segue.identifier isEqualToString:@"presentModelSegue"])
    {
        RDVPanoViewController* panoView = segue.destinationViewController;
        
        // Create a model
        panoView.model = [RDVModel new];
        
        // This might be a simple string
        if ([sender isKindOfClass:[NSString class]])
        {
            // Setup the model
            panoView.model.modelID = [sender uppercaseString];
        }
        // This might be a class
        else if ([sender isKindOfClass:[RDVModelInfo class]])
        {
            panoView.model.modelID = [sender modelCode];
        }
                
        // Notify
        [[FEAnalyticsManager sharedInstance] reportEvent:@"User requested model"
                                               eventData:@{@"ModelID": panoView.model.modelID}];
    }
    else if ([segue.identifier isEqualToString:@"loadModelSegue"])
    {        
        RDVLoadingViewController* loadingView = segue.destinationViewController;
        loadingView.modelInfo = sender;
    }
}

- (void) loadModelWithCode:(NSString*)modelCode
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Model Not Found On Server"
                                                    message:[NSString stringWithFormat:@"Please make sure the code '%@' is correct",modelCode]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
    
    // If all four character boxes are filled, perform segue
    if (modelCode.length > 0)
    {
        // Prepare load buttons
        self.modelTextField.enabled = NO;
        self.loadModelButton.enabled = NO;
        self.loadingIndicator.hidden = NO;
        [self.loadingIndicator startAnimating];
        
        // Check if the model exists
        [self.modelInfoRO loadModelWithModelID:[modelCode uppercaseString]
                                 andCompletion:^(RDVModelInfo* serverResponse)
         {
             // If the model wasn't found
             if ([serverResponse.state isEqualToString:@"Model not found"])
             {
                 [alert show];
             }
             // Present
             else
             {
                 // Check if we need to go to loading or skip
                 if (serverResponse.didFinishLoading ||
                     serverResponse.loaded_percent > 90 ||
                     serverResponse.filesize < 1000000)
                 {
                     [self performSegueWithIdentifier:@"presentModelSegue" sender:serverResponse];
                 }
                 // Loading required
                 else
                 {
                     [self performSegueWithIdentifier:@"loadModelSegue" sender:serverResponse];
                 }
             }
             
             // Reset load buttons
             self.modelTextField.enabled = YES;
             self.loadModelButton.enabled = YES;
             [self.loadingIndicator stopAnimating];
         }];
    }
    // Alert the user
    else
    {
        [alert show];
    }
}

- (IBAction)btnLoadModelPressed:(id)sender
{
    [self loadModelWithCode:self.modelTextField.text];
}

- (IBAction)backgroundTapped:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* const cellIdentifier = @"RDVRecentModelCell";
    RDVRecentModelCell* recentCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // Set the model
    recentCell.model = appData.recentModels[indexPath.row];
        
    return recentCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appData.recentModels.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Select this model
    RDVModel* recentModel = appData.recentModels[indexPath.row];
        
    // Present
    [self loadModelWithCode:recentModel.modelID];

}

@end
