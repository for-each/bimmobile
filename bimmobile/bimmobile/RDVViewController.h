//
//  RDVViewController.h
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FEAnalyticsManager.h>

@interface RDVViewController : UIViewController <FEAnalyticsManagerViewControllerProtocol>

- (void)displayLoading;
- (void)dismissLoading;

@end
