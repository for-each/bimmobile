//
//  RDVPanoViewController+Share.h
//  bimmobile
//
//  Created by Elad Lebovitch on 8/8/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVPanoViewController.h"

@interface RDVPanoViewController (Share)

// Sheet
- (void) presentShareActionSheet;

// Direct
- (void) shareToTwitter;
- (void) shareToFacebook;

@end
