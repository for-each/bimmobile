//
//  RDVPanoViewController+Share.m
//  bimmobile
//
//  Created by Elad Lebovitch on 8/8/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVPanoViewController+Share.h"
#import <FEFacebookManager.h>
#import <Twitter/Twitter.h>

@interface RDVPanoViewController () <UIActionSheetDelegate>

@end

@implementation RDVPanoViewController (Share)

- (void) presentShareActionSheet
{
    // Makw sure we don't create too many action sheets
    if (self.actionSheet == nil)
    {
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"Twitter", nil];
    }
    
    // Support iPad
    if (isiPad)
    {
        [self.actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    }
    else
    {
        [self.actionSheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Check button
    if  (buttonIndex == 0)
    {
        [self shareToFacebook];
    }
    else if (buttonIndex == 1)
    {
        [self shareToTwitter];
    }
}

- (void) shareToTwitter
{
    // Only if we can tweet
    if ([TWTweetComposeViewController canSendTweet])
    {
        // Create tweet
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        
        // Get the pano image
        UIImage* panoImagea = self.imageDictionary[[NSString stringWithRDVImageType:radiansToRDVType(self.panoView.hAngle)]];
        
        [tweetSheet addImage:panoImagea];
        
        // Getting the message / url from info.plist
        NSString* message = [NSBundle mainBundle].infoDictionary[@"RDVShareTextTwitter"];
        [tweetSheet setInitialText:message];
        [tweetSheet addURL:[NSURL URLWithString:[NSBundle mainBundle].infoDictionary[@"RDVShareURL"]]];
        
        // Present the tweet
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    // Could not tweet
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Could Not Tweet" message:@"Please make sure you are logged in to twitter via the device's settings app" delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}



- (void) shareToFacebook
{
    // Preventing double post issue
    static BOOL sharingToFB;
    
    // Should be restarted on each call
    sharingToFB = YES;
    
    // Share to FB
    [[FEFacebookManager sharedInstance] loginToFBWithCompletionHandler:^(BOOL success)
     {
         // If we are good
         if (success)
         {
             NSString* message = [NSBundle mainBundle].infoDictionary[@"RDVShareTextFB"];
             
             NSString* url = [self.currImagesURLs urlForRadians:self.panoView.hAngle].absoluteString;
             
             if (url && message)
             {
                 NSDictionary* params = @{@"message":message,
                                          @"url": url};
                 
                 // Make sure the user doens't double post
                 if (sharingToFB)
                 {
                     sharingToFB = NO;
                     
                     // Make sure we have permission
                     [[FEFacebookManager sharedInstance] requestPostPermissionsWithBlock:^(BOOL success)
                      {
                          if (success)
                          {
                              [FBRequestConnection startWithGraphPath:@"me/photos"
                                                           parameters:params
                                                           HTTPMethod:@"POST"
                                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                               {
                                   if (error == nil)
                                   {
                                       // Verify text
                                       UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Uploaded Successfully"
                                                                                       message:@"Thank you for sharing your experience with us"
                                                                                      delegate:nil
                                                                             cancelButtonTitle:@"OK"
                                                                             otherButtonTitles:nil];
                                       [alert show];
                                   }
                                   else
                                   {
                                       NSLog(@"Failed");
                                   }
                               }];
                              
                              
                          }
                      }];
                 }

             }
         };
     }];
}


@end
