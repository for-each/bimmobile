//
//  RDVRecentModelCell.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/2/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVModel.h"

@interface RDVRecentModelCell : UITableViewCell

@property (nonatomic, strong) RDVModel* model;

@end
