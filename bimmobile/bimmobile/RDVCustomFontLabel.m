#import "RDVCustomFontLabel.h"
#import <QuartzCore/QuartzCore.h>

@interface RDVCustomFontLabel ()
@property (copy, nonatomic) NSString *fontFamily;
@end

@implementation RDVCustomFontLabel

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:self.fontFamily size:self.font.pointSize];
}

@end
