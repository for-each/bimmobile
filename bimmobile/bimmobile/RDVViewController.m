//
//  RDVViewController.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVViewController.h"
#import <FEAnalyticsManager.h>

@interface RDVViewController ()

@property (nonatomic, strong) UIView* loadingView;
@end

@implementation RDVViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Report view will appear
    [[FEAnalyticsManager sharedInstance] reportViewWillAppear:self];
}

- (NSString *)viewControllerDisplayName
{
    return NSStringFromClass([self class]);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}


#pragma mark Loading

#pragma mark - Loading

- (void)displayLoading
{
    // Create one loading only
    if (self.loadingView == nil)
    {
        // Create a light gray overlay
        self.loadingView = [[UIView alloc] initWithFrame:self.view.bounds];
        self.loadingView.backgroundColor = [UIColor colorWithWhite:0.25 alpha:0.5];
        self.loadingView.userInteractionEnabled = YES;
        self.loadingView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        // Add an activity indecation
        UIActivityIndicatorView* indication = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.loadingView addSubview:indication];
        indication.center = CGPointMake(self.loadingView.frame.size.width / 2,
                                        self.loadingView.frame.size.height / 2);
        indication.autoresizingMask = UIViewAutoresizingNone;
        [indication startAnimating];
    }
    
    // Show the loading view
    self.loadingView.frame = self.view.bounds;
    
    [self.view addSubview:self.loadingView];
    [self.view bringSubviewToFront:self.loadingView];
}

- (void)dismissLoading
{
    [self.loadingView removeFromSuperview];
}

@end
