//
//  RDVMovementControlView.m
//  WOW
//
//  Created by Elad Lebovitch on 7/17/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import "RDVMovementControlView.h"
#import <QuartzCore/QuartzCore.h>


@implementation RDVMovementControlView

- (void)awakeFromNib
{
    self.layer.cornerRadius = 50;
}

- (IBAction)moveButtonTapped:(UIButton*)sender
{
    [self.delegate movementButtonTapped:sender.tag];
}

@end
