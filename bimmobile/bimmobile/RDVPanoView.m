//
//  RDVPanoView.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/25/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVPanoView.h"
#import <FELocationManager.h>

@implementation RDVPanoView

static CMMotionManager* motionManager;

#pragma mark - Core Motion
- (CMMotionManager *)motionManager
{
    if (motionManager == nil)
    {
        motionManager = [[CMMotionManager alloc] init];
    }
    
    return motionManager;
}

- (void)startAccelerometerUpdates
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(headingUpdated:) name:kFELocationManagerHeadingChangedNotification object:nil];
    
    // Detect device motion asynchronously
    [self.motionManager
     startDeviceMotionUpdatesToQueue:[[NSOperationQueue alloc] init]
     withHandler:^(CMDeviceMotion *motion, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(),
        ^{
            // If the enabled mode is set to MOTION VIEW, move the panoView
            if (self.allowGyroInteraction)
            {
                // Get values from device MOTION variable
                float roll = motion.attitude.roll;
                float pitch = motion.attitude.pitch;
                
                // Fix the angle
                const CGFloat fixAngle = 1.54;
                
                // If device is in LANDSCAPE (left/right), use ROLL, else use PITCH
                if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft )
                {
                    [self setVAngle:-roll - fixAngle];
                }
                else if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
                {
                    [self setVAngle:roll - fixAngle];
                }
                else
                {
                    // Check if device is tilted towards the user (user is trying to view the upper part of panoView)
                    if (motion.gravity.z <= 0)
                    {
                        [self setVAngle:pitch - fixAngle];
                    }
                    else
                    {
                        [self setVAngle:fixAngle - pitch];
                    }
                }
            }
        });
     }];
}

- (void)stopAccelerometerUpdates
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.motionManager stopDeviceMotionUpdates];
}

#pragma mark - Location Delegate
- (void) headingUpdated:(NSNotification*)notif
{
    CLHeading* newHeading = notif.userInfo[@"heading"];
    
    if (self.allowGyroInteraction)
    {
        // Convert Degree to Radian and move the panoView accordingly
        float newRad = DegreesToRadians(newHeading.magneticHeading);
        
        // setting the degrees
        CGFloat panoOffset = 0;
        
        // If device is in LANDSCAPE (left/right), use an offset
        if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft)
        {
            panoOffset = 90;
        }
        else if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
        {
            panoOffset = -90;
        }
        
        // Animate to avoid stutter
        [UIView animateWithDuration:0 animations:^
         {
             self.hAngle = newRad + DegreesToRadians(panoOffset);
         }];
    }
}

- (void)setAllowGyroInteraction:(BOOL)allowGyroInteraction
{
    _allowGyroInteraction = allowGyroInteraction;
    self.panEnabled = !allowGyroInteraction;
}

@end
