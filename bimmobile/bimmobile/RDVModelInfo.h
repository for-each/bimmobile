//
//  RDVModelInfo.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/17/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVBaseObject.h"
#import "RDVLocationPoint.h"

extern const NSString* TITLE;
extern const NSString* DESCRIPTION;
extern const NSString* LOGO;
extern const NSString* HTML1;
extern const NSString* HTML2;
extern const NSString* ACT1;
extern const NSString* ACT2;

@interface RDVModelInfo : RDVBaseObject

@property (nonatomic, strong) RDVLocationPoint* starting_point;
@property (nonatomic, strong) NSString* modelCode;
@property (nonatomic) NSInteger loaded_percent;
@property (nonatomic, strong) NSString* loading_message;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSArray* preview_urls;
@property (nonatomic) unsigned long long filesize;
@property (nonatomic) BOOL didFinishLoading;
@property (nonatomic, strong) NSArray* attributes;
@property (nonatomic, strong) NSMutableDictionary* modelDetails;

@end
