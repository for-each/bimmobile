//
//  RDVPanoView.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/25/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "JAPanoView.h"
#import <CoreMotion/CoreMotion.h>

@interface RDVPanoView : JAPanoView

- (void)startAccelerometerUpdates;
- (void)stopAccelerometerUpdates;

@property (nonatomic) BOOL allowGyroInteraction;
@property (nonatomic, readonly) CMMotionManager* motionManager;

@end
