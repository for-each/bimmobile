//
//  NSArray+RDVHelpers.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "NSArray+RDVHelpers.h"

@implementation NSArray (RDVHelpers)

+ (NSArray*) rdvImageTypesArray
{
    return @[@(RDVImageTypeNorth),@(RDVImageTypeEast),@(RDVImageTypeWest),@(RDVImageTypeTop), @(RDVImageTypeBottom), @(RDVImageTypeSouth)];
}

- (NSURL*)urlForType:(RDVImageType)type
{
    NSString* urlToReturn = nil;
    
    // Only if this is the number of images
    if (self.count == 6)
    {
        switch (type)
        {
            case RDVImageTypeWest:
            {
                urlToReturn = self[0];
                break;
            }
            case RDVImageTypeNorth:
            {
                urlToReturn = self[1];
                break;
            }
            case RDVImageTypeEast:
            {
                urlToReturn = self[2];
                break;
            }
            case RDVImageTypeSouth:
            {
                urlToReturn = self[3];
                break;
            }
            case RDVImageTypeTop:
            {
                urlToReturn = self[4];
                break;
            }
            case RDVImageTypeBottom:
            {
                urlToReturn = self[5];
                break;
            }
            default:
            {
                urlToReturn = nil;
                break;
            }
        }
    }
    
    if (urlToReturn == nil)
    {
        NSLog(@"ERROR - URL is nil for type %d", type);
    }
    NSURL* url = [NSURL URLWithString:[urlToReturn stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    if (url == nil)
    {
        NSLog(@"ERROR - URL is nil for string %@", urlToReturn);
    }
    
    return url;
}

- (NSURL*) urlForRadians:(CGFloat)radians
{
    return [self urlForType:radiansToRDVType(radians)];
}

@end
