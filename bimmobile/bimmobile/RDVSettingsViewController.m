//
//  RDVSettingsViewController.m
//  bimmobile
//
//  Created by Elad Lebovitch on 6/11/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVSettingsViewController.h"
#import <MessageUI/MessageUI.h>
#import <FEAnalyticsManager.h>

@interface RDVSettingsViewController () <MFMailComposeViewControllerDelegate, FEAnalyticsManagerViewControllerProtocol>
@property (weak, nonatomic) IBOutlet UITextField *altitudeText;
@property (weak, nonatomic) IBOutlet UISwitch *autoLocationUpdates;
@property (weak, nonatomic) IBOutlet UIButton *sendFeedbackButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *heightModeSegmented;
@property (weak, nonatomic) IBOutlet UISegmentedControl *imageSizeSegemented;
@property (weak, nonatomic) IBOutlet UISlider *altitudeSlider;
@end

@implementation RDVSettingsViewController

- (NSString *)viewControllerDisplayName
{
    return @"Settings View";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateAltitude];
    
    // Setup auto location updates
    [self.autoLocationUpdates setOn:appData.allowAutoLocationUpdates];
    
    // Enable feedback button only if sending is possible
    self.sendFeedbackButton.enabled = [MFMailComposeViewController canSendMail];
    [[FEAnalyticsManager sharedInstance] reportViewWillAppear:self];
}

- (void)viewDidUnload
{
    [self setAltitudeText:nil];
    [self setAltitudeSlider:nil];
    [self setImageSizeSegemented:nil];
    [self setHeightModeSegmented:nil];
    [self setSendFeedbackButton:nil];
    [super viewDidUnload];
}

- (void) updateAltitude
{
    // Set the altitude
    self.altitudeText.text = [NSString stringWithFormat:@"%d", appData.userHeight];
    
    // Using the user height
    if (self.altitudeSlider.value == 0)
    {
        self.altitudeText.text = [NSString stringWithFormat:@"GPS: %d", appData.userHeight];
    }
    
    // Choose the correct segment
    for (NSInteger currSegment = 0; currSegment < self.imageSizeSegemented.numberOfSegments; ++ currSegment)
    {
        // If this segment's title is the current one
        if ([self.imageSizeSegemented titleForSegmentAtIndex:currSegment].integerValue == appData.imageSize)
        {
            self.imageSizeSegemented.selectedSegmentIndex = currSegment;
        }
    }

    // Set the height mode
    self.heightModeSegmented.selectedSegmentIndex = appData.userHeightMode;
}
- (IBAction)autoLocationUpdatesChanged:(id)sender
{
    appData.allowAutoLocationUpdates = self.autoLocationUpdates.isOn;
}

- (IBAction)heightValueChanged:(id)sender
{
    if (self.altitudeSlider.value == 0)
    {
        appData.userHeight = appData.locationManager.lastKnownLocation.altitude;
    }
    else
    {
        appData.userHeight = self.altitudeSlider.value;
    }
    
    [self updateAltitude];
}

- (IBAction)textHeightEnded:(id)sender
{
    appData.userHeight = self.altitudeText.text.integerValue;
    self.altitudeSlider.value = appData.userHeight;
    [self.altitudeText resignFirstResponder];
    [self updateAltitude];
}
- (IBAction)editingDidEnd:(id)sender
{
    [self textHeightEnded:sender];
}

- (IBAction)donePressed:(id)sender
{
    [self.view endEditing:YES];
    [self.delegate settingsViewDidFinishWithChangeSettings:YES];
}

- (IBAction)imageSizeSelected:(id)sender
{
    appData.imageSize = [[self.imageSizeSegemented titleForSegmentAtIndex:self.imageSizeSegemented.selectedSegmentIndex] integerValue];
}

- (IBAction)heightModeChanged:(id)sender
{
    appData.userHeightMode = self.heightModeSegmented.selectedSegmentIndex;
}

#pragma mark MFMailCompose
- (IBAction)feedbackPressed:(id)sender
{
    MFMailComposeViewController* mailView = [MFMailComposeViewController new];
    [mailView setSubject:@"123Bim Mobile Feedback"];
    
    // Support email
    [mailView setToRecipients:@[@"support@rdvsystems.com"]];
    
    mailView.mailComposeDelegate = self;
    mailView.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mailView animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

@end
