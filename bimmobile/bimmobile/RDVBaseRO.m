//
//  RDVbaseRO.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVBaseRO.h"

@implementation RDVBaseRO

+ (NSString*)baseURL
{
    return  [NSBundle mainBundle].infoDictionary[@"RKServerBaseURL"];
}

+ (void) cancelAllRequests
{
    [[RKObjectManager sharedManager] cancelAllObjectRequestOperationsWithMethod:RKRequestMethodAny matchingPathPattern:@""];
}

@end
