//
//  RDVLoadingViewController.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVViewController.h"
#import "RDVModelInfo.h"

@interface RDVLoadingViewController : RDVViewController

@property (nonatomic, strong) RDVModelInfo* modelInfo;

@end
