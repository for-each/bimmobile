//
//  NSString+RDVHelpers.h
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "General.h"

@interface NSString (RDVHelpers)

+ (NSString*) stringWithRDVImageType:(RDVImageType)imageType;

@end
