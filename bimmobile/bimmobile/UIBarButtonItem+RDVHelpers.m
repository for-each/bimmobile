//
//  UIBarButtonItem+RDVHelpers.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "UIBarButtonItem+RDVHelpers.h"
#import <UIBarButtonItem+Additions.h>

@implementation UIBarButtonItem (RDVHelpers)

+ (UIBarButtonItem*) rdvBackBarButtonWithTarget:(id)target selector:(SEL)selector
{
    return [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"arrow_left"] target:target
                                      action:selector];
}

@end
