//
//  RDVImagesRO.h
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVBaseRO.h"

@interface RDVImagesRO : RDVBaseRO

- (void)uploadImage:(UIImage*)image
         forModelID:(NSString*)modelID
    imageDataParams:(NSDictionary*)imageInfoDict
     withCompletion:(FEROCompletionBlock)completion;

- (NSURL*)urlForModelID:(NSString*)modelID
             coordinate:(CLLocationCoordinate2D)coordinate
               altitude:(NSInteger)altitude
              imageSize:(NSInteger)size
              imageType:(RDVImageType)imageType
                  flags:(NSInteger)flags;

@end


