//
//  RDVPanoViewController.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVPanoViewController.h"
#import "JAPanoView.h"
#import "RDVImagesRO.h"
#import "RDVSettingsViewController.h"
#import "RDVModelInfoRO.h"
#import "RDVModelInfo.h"
#import <SDWebImage/SDWebImageManager.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMotion/CoreMotion.h>
#import <FELocationManager.h>
#import <UIBarButtonItem+Additions.h>

#define MOTION_UPDATE_INTERVAL 0.02

NSString* const NOTIF_UPDATE_PANO_VIEW = @"NOTIF_UPDATE_PANO_VIEW";

@interface RDVPanoViewController () <RDVSettingsViewDelegate>

@end

@implementation RDVPanoViewController

- (NSString *)viewControllerDisplayName
{
    return @"Panorama View";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Register for remote updates
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(panoNeedsUpdate) name:NOTIF_UPDATE_PANO_VIEW object:nil];
    
    // Setup the title
    self.title = [self.model.modelName stringByAppendingFormat:@" - %@",self.model.modelID];
    UIBarButtonItem* backButton = [UIBarButtonItem rdvBackBarButtonWithTarget:self.navigationController selector:@selector(popToRootViewControllerAnimated:)];
    
    UIBarButtonItem* refreshButton = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"Refresh.png"] target:self action:@selector(refreshPressed:)];
    self.navigationItem.leftBarButtonItems = @[backButton, refreshButton];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"pano_settings.png"] target:self action:@selector(settingsButtonTapped)];
        
    // Set the number of concurrent
    [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads = NUMBER_OF_IMAGES;
    
    // Present the image dictionary
    self.imageDictionary = [NSMutableDictionary dictionaryWithCapacity:NUMBER_OF_IMAGES];
    
    // Enable a tap gesture for full screen views:
    UITapGestureRecognizer* tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped)];
    [self.panoView addGestureRecognizer:tap];
    
    // Setup info view
    self.infoView.layer.cornerRadius = 4;
    self.infoView.alpha = 0;
        
    // Register for passthrough updates
    appData.locationManager.delegate = self;

    // Setup pano view
    self.panoView.motionManager.deviceMotionUpdateInterval = MOTION_UPDATE_INTERVAL;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatedGPSPosition)
                                                 name:kFELocationManagerLocationChangedNotification
                                               object:nil];
    
    // On load, start as "slave"
    [self btnUserInteractionTapped:nil];
    
    // And start loadingr
    [self loadModelInformation];
    
    // And add to recents
    [appData addRecentModel:self.model];
}

- (void) updateLocation
{
    self.longitudeLabel.text = [@(appData.locationManager.lastKnownLocation.coordinate.longitude) stringValue];
    self.latitudeLabel.text = [@(appData.locationManager.lastKnownLocation.coordinate.latitude) stringValue];
    self.altitudeLabel.text = [@(appData.userHeight) stringValue];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.panoView startAccelerometerUpdates];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.panoView stopAccelerometerUpdates];
    
    // Cancel next requests
    [RDVBaseRO cancelAllRequests];
}

- (void)viewDidUnload
{
    [self setPanoView:nil];
    [self setLoadingView:nil];
    [self setBtnUserInteraction:nil];
    [self setLoadingProgress:nil];
    [super viewDidUnload];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) showLoading:(BOOL)showLoading
{
    CGFloat fFrameY = 0;
    CGFloat fFrameAlpha = 1;
    
    if  (!showLoading)
    {
        fFrameY = -self.loadingView.frameHeight;
        fFrameAlpha = 0;
    }
    else
    {
        // Reset the progress
        self.loadingProgress.progress = 0;
    }
    
    // Show loading
    [UIView animateWithDuration:0.25 animations:^
    {
        self.loadingView.frameY = fFrameY;
        self.loadingView.alpha = fFrameAlpha;
    }];
}

- (void) loadModelInformation
{
    RDVModelInfoRO* modelInfo = [RDVModelInfoRO new];
    [modelInfo loadModelWithModelID:self.model.modelID andCompletion:^(RDVModelInfo* modelInfo)
    {
        // Save the model info
        self.modelInfo = modelInfo;
        
        // Check if I have a location point
        if (self.locationPoint == nil)
        {
            self.locationPoint = modelInfo.starting_point;
        }
        
        [self updateLocation];
        
        // Check if we are still loading
        if (modelInfo.didFinishLoading)
        {
            [self loadImagesForPano];
        }
        else
        {
            [self performSelector:@selector(loadModelInformation) withObject:nil afterDelay:1];
        }
    }];
}

- (void) cleanupPanoView
{
    [self.imageDictionary removeAllObjects];
    self.failedImages = 0;
}

- (void)loadImagesForPano
{
    // Don't load if already loading
    if (!self.nowLoading && self.modelInfo.loaded_percent == 100)
    {
        // Set the loading flag
        self.nowLoading = YES;
        [self cleanupPanoView];
                
        // Present loading
        [self showLoading:YES];
        
        // Use for getting images
        RDVImagesRO* imagesRO = [RDVImagesRO new];
        
        // Create an array of image types:
        NSArray* imageTypesArray = [NSArray rdvImageTypesArray];
        
        // Get the current coordinates
        CLLocationCoordinate2D coord = appData.locationManager.lastKnownLocation.coordinate;
        
        // Get the urls for the image types
        for (NSNumber* number in imageTypesArray)
        {
            // Create a single URL
            RDVImageType currType = number.integerValue;
            NSURL* url = [imagesRO urlForModelID:self.model.modelID
                                      coordinate:coord
                                        altitude:appData.userHeight
                                       imageSize:appData.imageSize
                                       imageType:currType
                                           flags:appData.userHeightMode];
            NSLog(@"Downlding url %@", url);
            
            // Download images
            [[SDWebImageManager sharedManager] downloadWithURL:url
                                                       options:SDWebImageRefreshCached
             progress:nil
             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
             {
                 // Make sure we got an image
                 if (error == nil && image != nil)
                 {
                     // Save the image to our dictionray
                     self.imageDictionary[[NSString stringWithRDVImageType:currType]] = image;
                     
                     [self.loadingProgress setProgress:(CGFloat)self.imageDictionary.allKeys.count /  (CGFloat)NUMBER_OF_IMAGES animated:YES];
                     
                     // Check and display the images
                     [self checkDownloadImagesComplete];
                 }
                 // Print error if not
                 else
                 {
                     // Add
                     ++self.failedImages;
                     
                     // If all images failed, got nothing to see
                     if (self.failedImages >= NUMBER_OF_IMAGES)
                     {
                         // Don't load again
                         self.nowLoading = NO;
                         
                         // Present error
                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error Loading Images" message:[NSString stringWithFormat:@"Could not load images for model %@", self.model.modelID ] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alert show];
                         [self.navigationController popViewControllerAnimated:YES];
                         
                         // Log to flurry
                         [[FEAnalyticsManager sharedInstance] logError:@"Load Images failed"
                                                               message:[NSString stringWithFormat:@"Model:%@", self.model.modelName]
                                                                 error:nil];
                     }
                     // Just log the error
                     else
                     {
                         NSLog(error.debugDescription,nil);
                     }
                 }
             }];
        }
    }
}

- (void) checkDownloadImagesComplete
{
    // Finished loading:
    if (self.imageDictionary.allKeys.count == NUMBER_OF_IMAGES && self.nowLoading)
    {
        // Setting all of our images from dictionary
        [self.panoView loadImagesWithRDVImagesDict:self.imageDictionary];

        // Hide loading
        [self showLoading:NO];
        [[FEAnalyticsManager sharedInstance] reportEvent:@"Pano Finished Loading Model"
                                               eventData:@{@"ModelID": self.model.modelID}];
        
        // We can now update the app data (the front image will be used)
        self.model.modelImage = self.imageDictionary[[NSString stringWithRDVImageType:RDVImageTypeNorth]];
        [appData addRecentModel:self.model];
        
        self.nowLoading = NO;
    }
}

#pragma mark movement
- (void)movementButtonTapped:(NSInteger)direction
{
    // Chaning to degrees and back (wasn't really needed, but used for debug)
    NSInteger degrees = ((NSInteger)RadiansToDegrees(self.panoView.hAngle));
    
    // Server is 90 degress off, so we take it off.
    degrees += (((direction - 1) * 90));
    
    // Not using verticals in that time
    self.moveToVector = CGPointMake(DegreesToRadians(degrees),
                                    0) ;
    
    // Reload the images
    [self loadImagesForPano];
}

#pragma mark Events
- (void) panoNeedsUpdate
{
    [self loadImagesForPano];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Check if this is settings
    if ([segue.identifier isEqualToString:@"RDVSettingsSegue"])
    {
        // Setting the presentation
        UINavigationController* naviControl = segue.destinationViewController;
        naviControl.modalPresentationStyle = UIModalPresentationFormSheet;
        
        // Setting the delegate
        RDVSettingsViewController* destination = (RDVSettingsViewController*)naviControl.topViewController;
        destination.delegate = segue.sourceViewController;
    }
}

- (void) settingsButtonTapped
{
    [self performSegueWithIdentifier:@"RDVSettingsSegue" sender:nil];
}

- (IBAction)refreshPressed:(id)sender
{
    // Hard refresh
    self.nowLoading = NO;

    // Don't download more images
    [[SDWebImageManager sharedManager] cancelAll];

    [self cleanupPanoView];
    [self loadModelInformation];
}

- (void) viewTapped
{
    // Toggels hide/show of top bars
    BOOL shouldHide = !self.navigationController.isNavigationBarHidden;
    
    [[UIApplication sharedApplication] setStatusBarHidden:shouldHide withAnimation:UIStatusBarAnimationSlide];
    [self.navigationController setNavigationBarHidden:shouldHide animated:YES];
}

- (IBAction)infoTapped:(id)sender
{
    self.infoButton.selected  = !self.infoButton.selected;
    
    // Fade in
    CGFloat infoAlpha = 0;
    if  (self.infoButton.selected)
    {
        infoAlpha = 1;
    }
    
    [UIView animateWithDuration:0.25 animations:^
    {
        self.infoView.alpha = infoAlpha;
    }];
    
    [[FEAnalyticsManager sharedInstance] reportEvent:@"Info Tapped" eventData:nil];
}

- (IBAction)btnUserInteractionTapped:(id)sender
{
    // Reverse selection
    self.btnUserInteraction.selected = !self.btnUserInteraction.selected;
    
    // Change the pan enabled state
    self.panoView.allowGyroInteraction = self.btnUserInteraction.selected;
}

#pragma mark - SettingsViewController
- (void)settingsViewDidFinishWithChangeSettings:(BOOL)didChangeSettings
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self loadModelInformation];
}

-(void)updatedGPSPosition
{
    // Only if auto updates are enabled
    if (appData.allowAutoLocationUpdates)
    {
        // Reload images if user moved to new location (traveled more than MIN_NUMBER_OF_METERS_FOR_NOTIF)
        [self loadImagesForPano];
    }
}

@end
