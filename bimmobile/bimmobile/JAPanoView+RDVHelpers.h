//
//  JAPanoView+RDVHelpers.h
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "JAPanoView.h"

@interface JAPanoView (RDVHelpers)

- (void) loadImagesWithRDVImagesDict:(NSDictionary*)imagesDict;

@end
