//
//  RDVModelInfo.m
//  bimmobile
//
//  Created by Elad Lebovitch on 6/17/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVModelInfo.h"

const NSString* TITLE = @"title";
const NSString* DESCRIPTION = @"description";
const NSString* LOGO = @"logo";
const NSString* HTML1 = @"html1";
const NSString* HTML2 = @"html2";
const NSString* ACT1 = @"act1";
const NSString* ACT2 = @"act2";


@implementation RDVModelInfo

+ (RKObjectMapping *)jsonMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[self class]];
    
    [mapping addAttributeMappingsFromArray:@[@"loaded_percent",
     @"loading_message",
     @"state", @"preview_urls", @"filesize", @"attributes"]];
    
    [mapping addRelationshipMappingWithSourceKeyPath:@"starting_point" mapping:[RDVLocationPoint jsonMapping]];
    
    return mapping;
}

- (void)setAttributes:(NSArray *)newAttributes
{
    if (newAttributes != nil)
    {
        _attributes = newAttributes;
        [self convertAttributes];
    }
}

- (BOOL)didFinishLoading
{
    return [self.state isEqualToString:@"Loaded"];
}

- (void)convertAttributes
{
    self.modelDetails = [NSMutableDictionary dictionary];
    
    // Convert the attributes to dictionary
    for (NSDictionary* dictionary in _attributes)
    {
        self.modelDetails[dictionary[@"Key"]] = dictionary[@"Value"];
    }
}

@end
