//
//  RDVMovementImages.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVMovementImagesRO.h"

@implementation RDVMovementImagesRO

static NSString* const modelMovementActionString = @"MoveToCubemapImageUV";

+(void)initialize
{
    [RDVBaseRO addResponseDescriptorForPathPattern:modelMovementActionString
                                       withMapping:[RDVMovementResponse jsonMapping]];
}

- (void) moveInModelID:(NSString*)modelID
          fromLocation:(RDVLocationPoint*)currLocation
           inDirection:(CGPoint)vector
         andCompletion:(FEROCompletionBlock)completion
{
    if (modelID && currLocation)
    {
        // Setup a responseComplete
        FEROResponseCompleteBlock responseComplete = ^(RKObjectRequestOperation *operation,
                                                       RKMappingResult *mappingResult)
        {
            // Get the user
            RDVMovementResponse* modelInfo = mappingResult.firstObject;
            
            // Call the completion block (if set)
            completion(modelInfo);
        };
        
        
        NSMutableDictionary* params = [@{@"ModelId": modelID,
                                       @"PosX":currLocation.X,
                                       @"PosY":currLocation.Y,
                                       @"PosZ":currLocation.Z,
                                       @"U":@(vector.y),
                                       @"V":@(vector.x)
                                       } mutableCopy];
        
        
        // Send data to server
        [self loadObjectWithParams:params withCompletionBlock:responseComplete];
    }
}

- (NSString *)actionString
{
    return modelMovementActionString;
}


@end

@implementation RDVGPSImagesRO

static NSString* const gpsMovementActionString = @"GetClosestCubemapImage";

+(void)initialize
{
    [RDVBaseRO addResponseDescriptorForPathPattern:gpsMovementActionString
                                       withMapping:[RDVMovementResponse jsonMapping]];
}


- (NSString *)actionString
{
    return gpsMovementActionString;
}

- (void) moveInModelID:(NSString*)modelID
         toGPSLocation:(CLLocation*)location
                 flags:(NSInteger)flags
         andCompletion:(FEROCompletionBlock)completion
{
    if (modelID)
    {
        // Setup a responseComplete
        FEROResponseCompleteBlock responseComplete = ^(RKObjectRequestOperation *operation,
                                                       RKMappingResult *mappingResult)
        {
            // Get the user
            RDVMovementResponse* modelInfo = mappingResult.firstObject;
            
            // Call the completion block (if set)
            completion(modelInfo);
        };
        
        // Set the coords
        NSMutableDictionary* params = [@{@"ModelId": modelID,
                                       @"Latitude":@(location.coordinate.latitude),
                                       @"Longitude":@(location.coordinate.longitude),
                                       @"Altitude":@(location.altitude),
                                       @"Flags":@(flags)
                                       } mutableCopy];
        
        // Send data to server
        [self loadObjectWithParams:params withCompletionBlock:responseComplete];
    }
}

@end


@implementation RDVMovementResponse

+ (RKObjectMapping *)jsonMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[self class]];
    
    [mapping addAttributeMappingsFromArray:@[@"images"]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"new_point"
                                                                                  toKeyPath:@"point"
                                                                                withMapping:[RDVLocationPoint jsonMapping]]];

    
    return mapping;
}

@end