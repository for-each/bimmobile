//
//  RDVbaseRO.h
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FEBaseRO.h>


@interface RDVBaseRO : FEBaseRO

+ (NSString*)baseURL;
+ (void) cancelAllRequests;

@end