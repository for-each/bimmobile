//
//  JAPanoView+RDVHelpers.m
//  bimmobile
//
//  Created by Elad Lebovitch on 7/10/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "JAPanoView+RDVHelpers.h"

@implementation JAPanoView (RDVHelpers)

- (void) loadImagesWithRDVImagesDict:(NSDictionary*)imagesDict
{
    [UIView transitionWithView:self
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         [self setFrontImage:imagesDict[[NSString stringWithRDVImageType:RDVImageTypeNorth]]
                  rightImage:imagesDict[[NSString stringWithRDVImageType:RDVImageTypeEast]]
                   backImage:imagesDict[[NSString stringWithRDVImageType:RDVImageTypeSouth]]
                   leftImage:imagesDict[[NSString stringWithRDVImageType:RDVImageTypeWest]]
                    topImage:imagesDict[[NSString stringWithRDVImageType:RDVImageTypeTop]]
                 bottomImage:imagesDict[[NSString stringWithRDVImageType:RDVImageTypeBottom]]];
     }
                    completion:nil];
}

@end
