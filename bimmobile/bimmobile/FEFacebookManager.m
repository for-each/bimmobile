//
//  FEFacebookManager.m
//  for-each
//
//  Created by Elad Lebovitch on 10/25/12.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FEFacebookManager.h"
#import <FEAnalyticsManager.h>

NSString *const FBSessionStateChangedNotification = @"FBSessionStateChangedNotification";

#pragma mark - Private

@interface FEFacebookManager ()

@end

#pragma mark - Implementation

@implementation FEFacebookManager

#pragma mark - Singleton

static FEFacebookManager* staticInstance;

+ (FEFacebookManager *)sharedInstance
{
    @synchronized(staticInstance)
    {
        if (staticInstance == nil)
        {
            staticInstance = [[FEFacebookManager alloc] init];
        }
    }
    
    return staticInstance;
}

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    return self;
}

#pragma mark - Facebook Integration Methods

- (void)loginToFBWithCompletionHandler:(FEFBLoginCompletionBlock)completion
{    
    NSArray *permissions = [NSArray arrayWithObjects:@"email", nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession* session, FBSessionState status, NSError* error)
    {
        // Notfiy changes
        [self sessionStateChanged:session state:status error:error];
        
         // Now update the user's details
         if (status == FBSessionStateOpen ||
             status == FBSessionStateOpenTokenExtended)
         {
             [self populateUserDetailsWithCompletion:completion];
         }
         else
         {
             // Tell our block if successful or not
             if (completion)
             {
                 completion(NO);
             }
             
             NSLog(@"Facebook error:%@", error.debugDescription);
         }
     }];
}

- (void)populateUserDetailsWithCompletion:(FEFBLoginCompletionBlock)completion
{
    // Setup a request with the current session
    FBRequest* request = [FBRequest requestForMe];
    
    [request startWithCompletionHandler:
     ^(FBRequestConnection *connection,
       NSDictionary<FBGraphUser> *user,
       NSError *error)
     {
         // Update user details anyway
         self.fbUserDetails = user;
         [self populateFriendsList];
         
         // Call the completion block (if set)
         if (completion)
         {
             completion(!error);
         }
     }];
}

- (void)populateFriendsList
{
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error)
    {
        // Sort the array
        NSMutableArray* array = [NSMutableArray arrayWithArray:[result objectForKey:@"data"]];
        [array sortUsingComparator:^NSComparisonResult(id<FBGraphUser> obj1, id<FBGraphUser> obj2)
        {
            return [obj1.first_name compare:obj2.first_name];
        }];
        
        self.friendsArray = array;
    }];
}

#pragma mark - Invite Friends

- (void)inviteFriendsDialog
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:nil];
    
    [self showInviteFriendsDialog:params];
}

- (void)inviteFriendDialog:(NSString *)userFacebookID
{
    NSMutableDictionary* params = [@{@"to": userFacebookID} mutableCopy];
    
    [self showInviteFriendsDialog:params]; 
}

- (void)showInviteFriendsDialog:(NSMutableDictionary *)params
{
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:@"Hi! Check out Quotes - awesome app to share your friends silly sayings and more!"
                                                    title:@"Invite Friends"
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL* resultURL, NSError* error)
     {
         if (error)
         {
             // Error sending request
         } else
         {
             if (result == FBWebDialogResultDialogNotCompleted)
             {
                 // User cancelled request
             }
             else
             {
                 // Request sent
                 [[FEAnalyticsManager sharedInstance] reportEvent:@"Facebook Invite Sent"
                                                        eventData:params];
             }
         }
     }];    
}

#pragma mark - Permissions

- (void)requestPostPermissionsWithBlock:(FEFBLoginCompletionBlock)block
{
    // Make sure we have publish
    if (![self.session.permissions containsObject:@"publish_stream"])
    {
        NSArray *permissions = [NSArray arrayWithObjects:@"publish_stream", nil];
        
        [self.session requestNewPublishPermissions:permissions
                                   defaultAudience:FBSessionDefaultAudienceFriends
                                 completionHandler:^(FBSession *session, NSError *error)
         {
             block(error == nil);
         }];
    }
    else
    {
        block(YES);
    }
}

#pragma mark - Overide Getters / Setters

- (BOOL)isConnected
{
    return [FBSession activeSession].isOpen ||
           [FBSession activeSession].state == FBSessionStateOpen ||
           [FBSession activeSession].state == FBSessionStateOpenTokenExtended;
}

- (FBSession *)session
{
    return [FBSession activeSession];
}

- (NSString *)accessToken
{
    return [FBSession activeSession].accessTokenData.accessToken;
}

// Callback for session changes.
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state)
    {
        case FBSessionStateOpen:
            if (!error)
            {
                // We have a valid session
                NSLog(@"User session found");
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification
                                                        object:session];
    
    if (error)
    {
        // Report error
        [[FEAnalyticsManager sharedInstance] reportEvent:@"Facebook Error"
                                               eventData:@{@"Error":error.debugDescription}];
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

// Opens a Facebook session and optionally shows the login UX.
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI
{
    return [FBSession openActiveSessionWithReadPermissions:nil
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error)
    {
        [self sessionStateChanged:session
                            state:state
                            error:error];
    }];
}

@end

@implementation NSString (FacbookAdditions)

- (NSDictionary*)parseURLParams
{
    NSString* query = self;
    
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init]
                                   ;
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}

@end

@implementation NSURL (FacebookAdditions)

+ (NSURL *)urlForProfileImageOfUserID:(long long)userID
{
    NSString *url = [NSString stringWithFormat:@"http://graph.facebook.com/%lld/picture?type=square", userID];
    return [NSURL URLWithString:url];
}

@end
