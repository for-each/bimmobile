//
//  RDVBaseObject.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/17/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RDVJsonMapping.h"

@interface RDVBaseObject : NSObject <RDVJsonMapping>

@end
