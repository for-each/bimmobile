//
//  RDVImagesRO.m
//  bimmobile
//
//  Created by Elad Lebovitch on 5/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "RDVImagesRO.h"

@interface RDVImagesRO () <NSURLConnectionDelegate>

@end

static NSString* const kUploadActionString = @"GetPathSignatureForModel";

@implementation RDVImagesRO

+(void)initialize
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NSMutableDictionary class]];
    [mapping addAttributeMappingsFromArray:@[@"access_key", @"bucket", @"filename", @"path",@"policy",@"signature"]];
    
    
    [RDVBaseRO addResponseDescriptorForPathPattern:kUploadActionString
                                       withMapping:mapping];
}

NSString* const kActionString = @"%@/GetCubemapImage?ModelId=%@&Latitude=%20.20f&Longitude=%20.20f&ImageSize=%d&ImageId=%c&Flags=%d&altitude=%d";

- (NSURL*)urlForModelID:(NSString*)modelID
             coordinate:(CLLocationCoordinate2D)coordinate
               altitude:(NSInteger)altitude
              imageSize:(NSInteger)size
              imageType:(RDVImageType)imageType
                  flags:(NSInteger)flags
{
    
    NSString* finalUrl = [NSString stringWithFormat:
                          kActionString,
                          [RDVBaseRO baseURL],
                          modelID,
                          coordinate.latitude,
                          coordinate.longitude,
                          size,
                          imageType,
                          flags,
                          altitude];
    return [NSURL URLWithString:finalUrl];
}

- (NSString *)actionString
{
    return kUploadActionString;
}

// Uploades the image in a 2-step process
- (void)uploadImage:(UIImage*)image
         forModelID:(NSString*)modelID
    imageDataParams:(NSDictionary*)imageInfoDict
     withCompletion:(FEROCompletionBlock)completion
{
    // Build the extra form fields
    NSString* formFields = @"";
    
    NSMutableDictionary* formFieldsDict = [NSMutableDictionary dictionaryWithCapacity:imageInfoDict.count];
    
    // Create the amazon form dict
    for (NSString* currKey in imageInfoDict.allKeys)
    {
        NSString* amazonKey = [NSString stringWithFormat:@"x-amz-meta-%@",currKey];

        // For this service
        formFields = [formFields stringByAppendingFormat:@"%@,", amazonKey];
        
        // For the next service
        formFieldsDict[amazonKey] = imageInfoDict[currKey];
    }
    
    // Remove the last ,
    if (formFields.length > 0)
    {
        formFields = [formFields
                      stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    }
    
    [self loadObjectWithParams:@{@"ModelId":modelID, @"SuccessUrl": @"success.html", @"FormFields":formFields}
           withCompletionBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
    {
        // Get the data
        NSDictionary* dict = mappingResult.dictionary.allValues[0];
       
        NSString* newPath = dict[@"path"];
        newPath = [newPath stringByAppendingPathComponent:@"${filename}"];
        
        // May need to use a different step here
        NSMutableDictionary* paramsDict = [@{
                                           @"AWSAccessKeyId": dict[@"access_key"],
                                            @"acl" : @"public-read",
                                           @"key": newPath,
                                            @"policy": dict[@"policy"],
                                            @"signature":dict[@"signature"],
                                           } mutableCopy];

        // Copy and alternate data from params dict
        [paramsDict addEntriesFromDictionary:formFieldsDict];
        
        RKObjectManager* manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:dict[@"bucket"]]];
        

        NSString* httpBucket = [NSString stringWithFormat:@"http://%@.s3.amazonaws.com", dict[@"bucket"]];
        NSLog(@"%@", httpBucket);
        
        // Upload the image
        NSMutableURLRequest* request = [manager multipartFormRequestWithObject:nil
                                                                        method:RKRequestMethodPOST
                                                                          path:httpBucket
                                                                    parameters:paramsDict
                                                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                        {
                                            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 1)
                                                                        name:@"file"
                                                                    fileName:dict[@"filename"]
                                                                    mimeType:@"image/jpeg"];
                                        }];
        
        // Upload
        RKObjectRequestOperation *uploadOperation = [manager objectRequestOperationWithRequest:request success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
        {
            // Upload complete
            completion(@(YES));
            
            // Probably need the url, or have it in the above
        }
        failure:^(RKObjectRequestOperation *operation, NSError *error)
        {
            NSLog(@"Filename is %@", dict[@"filename"]);
            NSLog(@"%@",paramsDict);
            NSLog(@"Failed: %@", error.debugDescription);
            completion(@(NO));
        }];
        
        [manager enqueueObjectRequestOperation:uploadOperation];
    }];
}



@end
