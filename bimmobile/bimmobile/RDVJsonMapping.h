//
//  RDVJsonMapping.h
//  bimmobile
//
//  Created by Elad Lebovitch on 6/17/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@protocol RDVJsonMapping <NSObject>

+ (RKObjectMapping*) jsonMapping;

@end
