//
//  RDVWebViewController.m
//  RDVViewer
//
//  Created by Elad Lebovitch on 8/11/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "RDVWebViewController.h"

@interface RDVWebViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation RDVWebViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Load the request
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.urlToLoad]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self displayLoading];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismissLoading];
}

- (IBAction)doneButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}
@end
