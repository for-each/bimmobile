//
//  VWPanoViewController.m
//  RDVViewer
//
//  Created by Elinor Exterman on 8/4/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "VWPanoViewController.h"
#import "RDVMovementImagesRO.h"
#import "RDVPanoViewController+Share.h"
#import "RDVWebViewController.h"
#import <SDWebImage/SDWebImageDownloader.h>
#import <SDWebImage/SDWebImageManager.h>

@interface VWPanoViewController () 

@property (nonatomic, strong) NSArray* currImagesURLs;
@property (weak, nonatomic) IBOutlet UIButton *moreActionsButton;

@end

@implementation VWPanoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.navigationItem.rightBarButtonItem = nil;
    
    // Creating the movement control
    self.movementControl = [[NSBundle mainBundle] loadNibNamed:@"RDVMovementControlView" owner:self options:nil][0];
    [self.view addSubview:self.movementControl];
    self.movementControl.frameBottomLeft = CGPointMake(10, self.view.frameHeight - 10);
    self.movementControl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    self.movementControl.delegate = self;
    
    // Check if to use location
    self.movementControl.hidden = appData.allowAutoLocationUpdates;
    
    // Create the action button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonPressed:)];
}

- (void) loadModelInformation
{
    RDVModelInfoRO* modelInfo = [RDVModelInfoRO new];
    [modelInfo loadModelWithModelID:self.model.modelID andCompletion:^(RDVModelInfo* modelInfo)
     {
         // Save the model info
         self.modelInfo = modelInfo;
         
         // Check if we need to setup our current location point
         if (self.locationPoint == nil)
         {
             if (modelInfo.starting_point.X.integerValue != 0 ||
                 modelInfo.starting_point.Y.integerValue != 0 ||
                 modelInfo.starting_point.Z.integerValue != 0)
             {
                 self.locationPoint = modelInfo.starting_point;
             }
             else
             {
                 UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error: Could Not Present Location"
                                                                 message:@"This is probably a temporary connection error, please try again at a later time."
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
             NSLog(@"%@",self.locationPoint);
         }
         
         // Load (this happens on first load)
         [self loadImagesForPano];
     }
                        loadOffline:YES];
}

- (void)loadImagesForPano
{    
    [[SDWebImageManager sharedManager] cancelAll];
    
    [self displayLoading];
    [self.imageDictionary removeAllObjects];
    
    FEROCompletionBlock completion = ^(RDVMovementResponse* movementResponse)
    {
        // Update our current point
        self.locationPoint = movementResponse.point;
        
        // Load the images
        [self loadImagesFromImageArray:movementResponse.images];
    };
    
    // Check if this normal movement
    if  (!appData.allowAutoLocationUpdates)
    {
        RDVMovementImagesRO* imagesRO = [RDVMovementImagesRO new];
        [imagesRO moveInModelID:self.modelInfo.modelCode
                   fromLocation:self.locationPoint
                    inDirection:self.moveToVector
                  andCompletion:completion];
    }
    // This is the gps movement
    else
    {
        // Do GPS movement
        RDVGPSImagesRO* imagesRO = [RDVGPSImagesRO new];
        [imagesRO moveInModelID:self.modelInfo.modelCode
                  toGPSLocation:appData.locationManager.lastKnownLocation
                          flags:0
                  andCompletion:completion];
    }
}

- (void) loadImagesFromImageArray:(NSArray*)imageArray
{
    // Update the currImagesArray
    self.currImagesURLs = imageArray;
    
    // Make sure we got images
    if (imageArray.count == 0)
    {
        NSLog(@"An error occured: No images received");
        [self dismissLoading];
    }
    else
    {
        // Download all of the images
        for (NSNumber* number in [NSArray rdvImageTypesArray])
        {
            // Getting the value
            RDVImageType type = number.integerValue;
            NSURL* url = [self.currImagesURLs urlForType:type];
            
            // Download this image
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:url
                                                                  options:SDWebImageDownloaderIgnoreCachedResponse
                                                                 progress:nil
                                                                completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
             {
                 // Only if no error
                 if (error == nil && image != nil)
                 {
                     self.imageDictionary[[NSString stringWithRDVImageType:type]] = image;
                     
                     // If we are finished loading
                     if (self.imageDictionary.allKeys.count == NUMBER_OF_IMAGES)
                     {
                         [self.panoView loadImagesWithRDVImagesDict:self.imageDictionary];
                         [self dismissLoading];
                     }
                 }
                 else
                 {
                     NSLog(@"An error occured: %@", error.debugDescription);
                     [self dismissLoading];
                 }
             }];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure we are looking at an action
    if ([segue.identifier isEqualToString:@"VWActionSegue"])
    {
        // Get the navigation controller
        UINavigationController* controller = segue.destinationViewController;
        
        // Set the url
        RDVWebViewController* webViewController = (RDVWebViewController*)controller.topViewController;
        webViewController.urlToLoad = [NSURL URLWithString:self.modelInfo.modelDetails[ACT1]];
        webViewController.title = self.modelInfo.modelCode;
    }
}

#pragma mark User Action
- (IBAction)actionButtonPressed:(id)sender
{
    [self presentShareActionSheet];
}


- (void)viewDidUnload {
    [self setMoreActionsButton:nil];
    [super viewDidUnload];
}
@end
