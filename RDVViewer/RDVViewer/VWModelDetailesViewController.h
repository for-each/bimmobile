//
//  VWModelDetailesViewController.h
//  RDVViewer
//
//  Created by Elinor Exterman on 7/30/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VWModelDetailesViewController : UIViewController

@property (strong, nonatomic) NSString* ModelId;


@end
