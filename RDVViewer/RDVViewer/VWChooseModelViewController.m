//
//  VWChooseModelViewController.m
//  RDVViewer
//
//  Created by Elinor Exterman on 7/30/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "VWChooseModelViewController.h"
#import "VWAppData.h"
#import "RDVPanoViewController.h"
#import "RDVRecentModelCell.h"

@implementation VWChooseModelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CheckModeId) name:@"AppLaunchedFromURL" object:nil];
}

- (void)CheckModeId
{
    if ([VWAppData instance].ModelId != nil)
    {
        [self performSegueWithIdentifier:@"DetailsSegue" sender:self];
    }
}

- (IBAction)btnLoadModelPressed:(id)sender
{
//    [super btnLoadModelPressed:sender];
    [self loadModelWithCode:self.modelTextField.text];
}

- (void) loadModelWithCode:(NSString*)modelCode
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Model Not Found On Server"
                                                    message:[NSString stringWithFormat:@"Please make sure the code '%@' is correct",modelCode]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
    
    // If all four character boxes are filled, perform segue
    if (modelCode.length > 0)
    {
        // Prepare load buttons
        self.modelTextField.enabled = NO;
        self.loadModelButton.enabled = NO;
        self.loadingIndicator.hidden = NO;
        [self.loadingIndicator startAnimating];
        
        // Check if the model exists
        [self.modelInfoRO loadModelWithModelID:[modelCode uppercaseString]
                                 andCompletion:^(RDVModelInfo* serverResponse)
         {
             // If the model wasn't found
             if ([serverResponse.state isEqualToString:@"Model not found"])
             {
                 [alert show];
             }
             // Present
             else
             {
                 [VWAppData instance].ModelId = serverResponse.modelCode;
                     [self performSegueWithIdentifier:@"DetailsSegue" sender:serverResponse];
             }
             
             // Reset load buttons
             self.modelTextField.enabled = YES;
             self.loadModelButton.enabled = YES;
             [self.loadingIndicator stopAnimating];
         }];
    }
    // Alert the user
    else
    {
        [alert show];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppLaunchedFromURL" object:nil];
}
@end
