//
//  VWAppData.m
//  RDVViewer
//
//  Created by Elinor Exterman on 7/30/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "VWAppData.h"

static VWAppData* AppData = nil;

@implementation VWAppData

+ (VWAppData*)instance
{
    if (AppData == nil)
    {
        AppData = [VWAppData new];
    }
    
    return AppData;
}

@end
