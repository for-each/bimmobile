//
//  AppDelegate.m
//  RDVViewer
//
//  Created by Elinor Exterman on 7/30/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "AppDelegate.h"
#import "VWAppData.h"
#import <Facebook.h>
#import <TestFlight.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    NSString* serverURL = [NSBundle mainBundle].infoDictionary[@"RKServerBaseURL"];
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:serverURL]];
    [RKObjectManager setSharedManager:objectManager];

#ifdef ADHOC
    // Using testflight
    [TestFlight takeOff:@"4a65142d-896f-43c4-8a62-e6934fa77832"];

    // Rest kit
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    
#endif
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    if ([url.absoluteString hasPrefix:@"fb"])
    {
        return [[FBSession activeSession] handleOpenURL:url];
    }
    else
    {
        NSLog(@"URL: %@",[url query]);
        NSMutableDictionary* dictParams = [self DictionaryForUrl:url];
        
        [VWAppData instance].ModelId = [dictParams objectForKey:@"ModelId"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppLaunchedFromURL" object:self userInfo:nil];
        return YES;
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (NSMutableDictionary*)DictionaryForUrl:(NSURL*)url
{
    NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
    NSArray *components = [[url query] componentsSeparatedByString:@"&"];
    
    for (NSString *component in components) {
        NSArray *pair = [component componentsSeparatedByString:@"="];
        
        [queryParams setObject:[[pair objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding: NSMacOSRomanStringEncoding] forKey:[pair objectAtIndex:0]];
    }
    
    return queryParams;
}

@end
