//
//  VWAppData.h
//  RDVViewer
//
//  Created by Elinor Exterman on 7/30/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VWAppData : NSObject

@property NSString* ModelId;

+ (VWAppData*)instance;

@end
