//
//  RDVWebViewController.h
//  RDVViewer
//
//  Created by Elad Lebovitch on 8/11/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "RDVViewController.h"

@interface RDVWebViewController : RDVViewController

@property (nonatomic, strong) NSURL* urlToLoad;

@end
