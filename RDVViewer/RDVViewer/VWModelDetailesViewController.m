//
//  VWModelDetailesViewController.m
//  RDVViewer
//
//  Created by Elinor Exterman on 7/30/13.
//  Copyright (c) 2013 Elinor Exterman. All rights reserved.
//

#import "VWModelDetailesViewController.h"
#import "RDVModelInfoRO.h"
#import "VWAppData.h"
#import "RDVPanoViewController.h"

@interface VWModelDetailesViewController ()

@property (strong, nonatomic) RDVModelInfoRO* modelInfoRO;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)btnGPSMovmentPressed:(id)sender;
- (IBAction)btnNormalMovmentPressed:(id)sender;
@end

@implementation VWModelDetailesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.modelInfoRO = [RDVModelInfoRO new];
    self.ModelId = [VWAppData instance].ModelId;
    
    UIBarButtonItem* backButton = [UIBarButtonItem rdvBackBarButtonWithTarget:self.navigationController selector:@selector(popToRootViewControllerAnimated:)];
	self.navigationItem.leftBarButtonItem = backButton;
    
    [self GetModel];
}

- (void) GetModel
{
    // Check if the model exists
    [self.modelInfoRO loadModelWithModelID:self.ModelId InfoOnly:YES loadOffline:NO
                             andCompletion:^(RDVModelInfo* serverResponse)
     {
         // If the model wasn't found
         if ([serverResponse.state isEqualToString:@"Model not found"])
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Model Not Found On Server"
                                                             message:[NSString stringWithFormat:@"Please make sure the code '%@' is correct",self.ModelId]
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
             
             [alert show];
         }
         // Present
         else
         {
             // Get the title
             self.navigationItem.title = serverResponse.modelDetails[TITLE];
             
             //pass the string to the webview
             [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:serverResponse.modelDetails[HTML1]]]];
             
             //add it to the subview
             [self.view addSubview:self.webView];
         }
     }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    RDVPanoViewController* panoView = segue.destinationViewController;
    
    // Create a model
    panoView.model = [RDVModel new];

    // Setup the model
    panoView.model.modelID = self.ModelId;
}

- (IBAction)btnGPSMovmentPressed:(id)sender
{
    appData.allowAutoLocationUpdates = YES;
    [self performSegueWithIdentifier:@"PanoSegue" sender:nil];
}

- (IBAction)btnNormalMovmentPressed:(id)sender
{
    appData.allowAutoLocationUpdates = NO;
    [self performSegueWithIdentifier:@"PanoSegue" sender:nil];
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}
@end
