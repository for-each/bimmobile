//
//  WOWPortraitNaviViewController.h
//  WOW
//
//  Created by Elad Lebovitch on 7/22/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOWPortraitNaviViewController : UINavigationController

@end
