//
//  main.m
//  WOW
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WOWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WOWAppDelegate class]));
    }
}
