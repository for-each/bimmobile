//
//  FEPurchaseManager.m
//  Piano
//
//  Created by Elinor Exterman on 11/21/12.
//
//

#import "FEPurchaseManager.h"
static FEPurchaseManager* manager = nil;

@implementation FEPurchaseManager

+ (FEPurchaseManager*)instance
{
    if (manager == nil)
    {
        manager = [[FEPurchaseManager alloc]init];
    }
    
    return manager;
}

- (void)LoadStore
{
    self.request = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kInAppPurchaseManagerProductsFetchedNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(PurchaseResponse)
                                                 name:kInAppPurchaseManagerProductsFetchedNotification object:nil];
    
    // Restarts any purchases if they were interrupted last time the app was open
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    // get the product description (defined in early sections)
    [self requestProductData];
}

- (void) requestProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:self.productIdentifier];
    SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers:
                                 productIdentifiers];
    request.delegate = self;
    [request start];
}

- (void) PurchaseResponse
{
    // Check if we can make the purchase
    if ([self CanMakePurchases])
    {
        [self PerformPurchase];
    }
}

- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    // Get the product or change it to get all products
    NSArray *arrProducts = response.products;
    self.product = (arrProducts.count > 0 ? [arrProducts objectAtIndex:0] : nil);
    
    if (self.product)
    {
        NSLog(@"Product title: %@" , self.product.localizedTitle);
        NSLog(@"Product description: %@" , self.product.localizedDescription);
        NSLog(@"Product price: %@" , self.product.price);
        NSLog(@"Product id: %@" , self.product.productIdentifier);
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:nil];
    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers)
    {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification
                                                        object:self
                                                      userInfo:nil];
}


// Call this before making a purchase
- (BOOL)CanMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

// kick off the upgrade transaction
- (void) PerformPurchase
{
    // Check that there is product to purchase
    if (self.product)
    {
        // Perform purchase with the selected product
        SKPayment *payment = [SKPayment paymentWithProduct:self.product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}

#pragma -
#pragma Purchase helpers

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    [self recordTransaction:transaction.originalTransaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    if ([transaction.payment.productIdentifier isEqualToString:self.productIdentifier])
    {
        // save the transaction receipt to disk
        NSString* strReceipt = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
        
        strReceipt = [self base64String:strReceipt];
        
        [[NSUserDefaults standardUserDefaults] setValue:strReceipt forKey:[NSString stringWithFormat:@"PurchaseTransactionReceipt-%@", self.productIdentifier] ];
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionIdentifier forKey:[NSString stringWithFormat:@"PurchaseTransactionIdentifier-%@", self.productIdentifier] ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// enable features
//
- (void)provideContent:(NSString *)productId
{
    if ([productId isEqualToString:self.productIdentifier])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:self.productIdentifier];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    
    NSString* strReceipt = @"";
    
    if (transaction.transactionReceipt)
    {
        strReceipt = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
        
        strReceipt = [self base64String:strReceipt];
    }
    
    NSDictionary *userInfo = @{@"transaction":transaction,
                               @"receipt":strReceipt};
    
    if (wasSuccessful)
    {
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];
    }
    else
    {
        NSLog(@"Error: %@. %@", transaction.error.debugDescription, transaction.error.description);
        
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
    }
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        // error!
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
        
        // send out a notification for the canceled transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionCanceledNotification object:self userInfo:userInfo];
    }
}

#pragma mark - SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{

    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        if (transaction.transactionState == SKPaymentTransactionStatePurchased ||
            transaction.transactionState == SKPaymentTransactionStateRestored)
        {
            [self provideContent:transaction.payment.productIdentifier];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerRestoreFinishedNotification object:self];

}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"Restore error: %@", error.description);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerRestoreFinishedNotification object:self];
}

- (void)RestoreTransaction
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark Base64

- (NSString *)base64String:(NSString *)str
{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

@end


