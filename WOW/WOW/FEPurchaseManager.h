//
//  FEPurchaseManager.h
//  Piano
//
//  Created by Elinor Exterman on 11/21/12.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

// Notifications
#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"
#define kInAppPurchaseManagerTransactionCanceledNotification @"kInAppPurchaseManagerTransactionCanceledNotification"
#define kInAppPurchaseManagerRestoreFinishedNotification @"kInAppPurchaseManagerRestoreFinishedNotification"

@interface FEPurchaseManager : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (strong, nonatomic) SKProduct* product;
@property (strong, nonatomic) SKProductsRequest* request;
@property (strong, nonatomic) NSString* productIdentifier;

+ (FEPurchaseManager*)instance;
- (void) LoadStore;
- (BOOL) CanMakePurchases;
- (void) PerformPurchase;
- (void)RestoreTransaction;

@end
