//
//  WOWWelcomeViewController.h
//  WOW
//
//  Created by Elad Lebovitch on 7/22/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVViewController.h"

@interface WOWWelcomeViewController : RDVViewController

@end
