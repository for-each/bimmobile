//
//  WOWPortraitNaviViewController.m
//  WOW
//
//  Created by Elad Lebovitch on 7/22/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import "WOWPortraitNaviViewController.h"

@interface WOWPortraitNaviViewController ()

@end

@implementation WOWPortraitNaviViewController

// Used to disallow landscape
- (NSUInteger)supportedInterfaceOrientations
{
    if (isiPad)
    {
        return UIInterfaceOrientationMaskAll;
    }
    else
    {
        return UIInterfaceOrientationMaskPortrait;
    }
}


@end
