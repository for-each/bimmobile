//
//  WOWWelcomeViewController.m
//  WOW
//
//  Created by Elad Lebovitch on 7/22/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import "WOWWelcomeViewController.h"

@interface WOWWelcomeViewController ()

@end

@implementation WOWWelcomeViewController

- (IBAction)cancelButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return isiPad || UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}


@end
