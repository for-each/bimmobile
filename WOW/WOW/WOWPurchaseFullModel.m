//
//  WOWPurchaseFullModel.m
//  WOW
//
//  Created by Elad Lebovitch on 7/23/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import "WOWPurchaseFullModel.h"
#import "FEPurchaseManager.h"

@interface WOWPurchaseFullModel ()
@property (weak, nonatomic) IBOutlet UIButton *purchaseButton;

@end

@implementation WOWPurchaseFullModel

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseSuccessful) name:kInAppPurchaseManagerTransactionSucceededNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseFailed) name:kInAppPurchaseManagerTransactionFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissLoading) name:kInAppPurchaseManagerRestoreFinishedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissLoading) name:kInAppPurchaseManagerTransactionCanceledNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark Events
- (IBAction)cancelButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)purchaseModelButtonPressed:(id)sender
{
    // Make the purchase
    [FEPurchaseManager instance].productIdentifier = [NSBundle mainBundle].infoDictionary[@"HDModel"];
    [[FEPurchaseManager instance] LoadStore];
    
    [self displayLoading];
}

- (IBAction)restorePurchases:(id)sender
{
    [FEPurchaseManager instance].productIdentifier = [NSBundle mainBundle].infoDictionary[@"HDModel"];
    [[FEPurchaseManager instance] RestoreTransaction];
    
    [self displayLoading];
}

- (void) purchaseSuccessful
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Thank You" message:[NSBundle mainBundle].infoDictionary[@"ModelPurchasedText"]
                                                   delegate:nil cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [self dismissLoading];
    
    // Update the pano
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_UPDATE_PANO_VIEW object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) purchaseFailed
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Could not process purchase" message:@"Please try again later, make sure in-app-purchases are enabled in your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self dismissLoading];
}

- (void)viewDidUnload
{
    [self setPurchaseButton:nil];
    [super viewDidUnload];
}
@end
