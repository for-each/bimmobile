//
//  WOWPanoViewController.m
//  WOW
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import "WOWPanoViewController.h"
#import "RDVModelInfoRO.h"
#import <SDWebImage/SDWebImageDownloader.h>
#import <SDWebImage/SDWebImageManager.h>
#import "RDVMovementImagesRO.h"
#import "NSArray+RDVHelpers.h"
#import "RDVPanoViewController+Share.h"



@interface WOWPanoViewController () <JAPanoViewDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UILabel *debugLabel;
@property (weak, nonatomic) IBOutlet UIButton *fullModelButton;
@property (weak, nonatomic) IBOutlet UIButton *standWithUsButton;
@property (weak, nonatomic) IBOutlet UIButton *allowGPS;
@property (weak, nonatomic) IBOutlet UILabel *upgradeLabel;
@property (weak, nonatomic) IBOutlet UIView *upgradeContainer;

@end

@implementation WOWPanoViewController

- (void) updateModelType
{
    // This is a free model
    self.model = [RDVModel new];
    
    self.fullModelButton.hidden = YES;
    self.standWithUsButton.hidden = YES;
    
    // Check if we have purchased the pro model
    if  ([[NSUserDefaults standardUserDefaults] boolForKey:[NSBundle mainBundle].infoDictionary[@"HDModel"]])
    {
        // Change model code:
        self.model.modelID = [NSBundle mainBundle].infoDictionary[@"PaidModelID"];
        self.standWithUsButton.hidden = NO;
        
        // If this model is disabeld - don't show this button
        if ([NSBundle mainBundle].infoDictionary[@"PermUserModel"] == nil)
        {
            // Hide the button
            self.upgradeContainer.hidden = YES;
        }
        // Show the button according to the user purchase status
        else
        {
            // Uncommenting this row allows the user to upload only a single image at a time
            // self.upgradeContainer.hidden = [[NSUserDefaults standardUserDefaults] boolForKey:[NSBundle mainBundle].infoDictionary[WOW_IAP_PERMMODEL]];
            self.upgradeContainer.hidden = NO;
            self.upgradeLabel.text = [NSBundle mainBundle].infoDictionary[@"uploadModelButtonText"];
        }
    }
    else
    {
        self.model.modelID = [NSBundle mainBundle].infoDictionary[@"FreeModelID"];
        self.fullModelButton.hidden = NO;
        self.upgradeLabel.text = [NSBundle mainBundle].infoDictionary[@"upgradeModelButtonText"];
    }
    
    // Update the mode info's code if needed
    self.modelInfo.modelCode = self.model.modelID;
}

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    // Must happen before viewDidLoad
    [self updateModelType];
    self.fullModelButton.titleLabel.transform =  CGAffineTransformMakeRotation(M_PI_4);
    
    // This is supposed to happen after a model is chosen
    [super viewDidLoad];
    
    // Creating the movement control
    self.movementControl = [[NSBundle mainBundle] loadNibNamed:@"RDVMovementControlView" owner:self options:nil][0];
    [self.view addSubview:self.movementControl];
    self.movementControl.frameBottomLeft = CGPointMake(10, self.view.frameHeight - 10);
    self.movementControl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    self.movementControl.delegate = self;
    
    // Create the action button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonPressed:)];
    
    self.upgradeLabel.transform = CGAffineTransformMakeRotation(DegreesToRadians(46));
}

- (void)viewDidUnload
{
    [self setDebugLabel:nil];
    [self setFullModelButton:nil];
    [self setStandWithUsButton:nil];
    [self setAllowGPS:nil];
    [self setUpgradeLabel:nil];
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController* viewController = segue.destinationViewController;
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
}

- (void) loadModelInformation
{
    RDVModelInfoRO* modelInfo = [RDVModelInfoRO new];
    [modelInfo loadModelWithModelID:self.model.modelID andCompletion:^(RDVModelInfo* modelInfo)
     {
         // Save the model info
         self.modelInfo = modelInfo;
         
         // Check if we need to setup our current location point
         if (self.locationPoint == nil)
         {
             self.locationPoint = modelInfo.starting_point;
             NSLog(@"%@",self.locationPoint);
         }
         
         // Load (this happens on first load)
         [self loadImagesForPano];
     }
                        loadOffline:YES];
}

// After an IAP was made
- (void) panoNeedsUpdate
{
    // Reset everything
    self.locationPoint = nil;
    [self updateModelType];
    [self loadModelInformation];
}

- (void)loadImagesForPano
{
    // Update model info
    [self updateModelType];
    
    [[SDWebImageManager sharedManager] cancelAll];
    
    [self displayLoading];
    [self.imageDictionary removeAllObjects];
    
    FEROCompletionBlock completion = ^(RDVMovementResponse* movementResponse)
    {
        // Update our current point
        self.locationPoint = movementResponse.point;
        
        // Load the images
        [self loadImagesFromImageArray:movementResponse.images];
    };
    
    // Check if this normal movement
    if  (!appData.allowAutoLocationUpdates)
    {
        RDVMovementImagesRO* imagesRO = [RDVMovementImagesRO new];
        [imagesRO moveInModelID:self.modelInfo.modelCode
                   fromLocation:self.locationPoint
                    inDirection:self.moveToVector
                  andCompletion:completion];
    }
    // This is the gps movement
    else
    {
        // Do GPS movement
        RDVGPSImagesRO* imagesRO = [RDVGPSImagesRO new];
        [imagesRO moveInModelID:self.modelInfo.modelCode
                  toGPSLocation:appData.locationManager.lastKnownLocation
                          flags:0
                  andCompletion:completion];
    }
}

- (void) loadImagesFromImageArray:(NSArray*)imageArray
{
    // Update the currImagesArray
    self.currImagesURLs = imageArray;
    
    // Make sure we got images
    if (imageArray.count == 0)
    {
        NSLog(@"An error occured: No images received");
        [self dismissLoading];
    }
    else
    {
        // Download all of the images
        for (NSNumber* number in [NSArray rdvImageTypesArray])
        {
            // Getting the value
            RDVImageType type = number.integerValue;
            NSURL* url = [self.currImagesURLs urlForType:type];
            
            // Download this image
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:url
                                                                  options:SDWebImageDownloaderIgnoreCachedResponse
                                                                 progress:nil
                                                                completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
             {
                 // Only if no error
                 if (error == nil && image != nil)
                 {
                     self.imageDictionary[[NSString stringWithRDVImageType:type]] = image;
                     
                     // If we are finished loading
                     if (self.imageDictionary.allKeys.count == NUMBER_OF_IMAGES)
                     {
                         [self.panoView loadImagesWithRDVImagesDict:self.imageDictionary];
                         [self dismissLoading];
                     }
                 }
                 else
                 {
                     NSLog(@"An error occured: %@", error.debugDescription);
                     [self dismissLoading];
                 }
             }];
        }
    }
}

- (IBAction)actionButtonPressed:(id)sender
{
    [self presentShareActionSheet];
}

- (IBAction)gpsButtonPressed:(id)sender
{
    self.allowGPS.selected = !self.allowGPS.selected;
    appData.allowAutoLocationUpdates = self.allowGPS.selected;
    
    // Hide the movement controls
    self.movementControl.hidden = appData.allowAutoLocationUpdates;
    
    // Update GPS location now
    [self updatedGPSPosition];
}

#pragma mark workaround for server heading
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    [super locationManager:manager didUpdateHeading:newHeading];
    
    // Hard coded 180 degrees fix
    if (self.btnUserInteraction.selected)
    {
        self.panoView.hAngle += DegreesToRadians(180);
    }
}

#pragma mark Unwind Segue
- (IBAction)cancelPressed:(UIStoryboardSegue *)segue
{
    // Optional place to read data from closing controller
}

@end
