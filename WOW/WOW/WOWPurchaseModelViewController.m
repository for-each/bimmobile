//
//  WOWPurchaseModelViewController.m
//  WOW
//
//  Created by Elad Lebovitch on 7/22/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import "WOWPurchaseModelViewController.h"
#import "RDVImagesRO.h"
#import "FEPurchaseManager.h"

@interface WOWPurchaseModelViewController () <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UITextField *userEmail;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *userGender;
@property (weak, nonatomic) IBOutlet UISlider *userHeightSlider;
@property (weak, nonatomic) IBOutlet UILabel *userHeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *userImageLabel;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (nonatomic, strong) UIPopoverController* popover;

@end

@implementation WOWPurchaseModelViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseSuccessful:) name:kInAppPurchaseManagerTransactionSucceededNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseFailed) name:kInAppPurchaseManagerTransactionFailedNotification object:nil];
    [self sliderValueChanged:self.userHeightSlider];
}

#pragma mark In App Purchases
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)restorePressed:(id)sender
{
    
}

- (void) purchaseSuccessful:(NSNotification*)notif
{
    // Get the transaction data
    SKPaymentTransaction* transaction = notif.userInfo[@"transaction"];
    
    NSString* transIdentifier = @"";
//    NSString* transReceipt = notif.userInfo[@"receipt"];
    
    // Add data
    if (transaction.transactionIdentifier)
    {
        transIdentifier = transaction.transactionIdentifier;
    }
    
    RDVImagesRO* imagesRO =[RDVImagesRO new];
    
    // Check if Male or female
    NSString* genderString = nil;
    if (self.userGender.selectedSegmentIndex == 1)
    {
        genderString = @"Male";
    }
    else
    {
        genderString = @"Female";
    }
    
    // Upload for image
    [imagesRO uploadImage:self.userImage.image
               forModelID:[NSBundle mainBundle].infoDictionary[@"PaidModelID"]
          imageDataParams:@{@"email":self.userEmail.text,
     @"name" : self.userName.text,
     @"gender": genderString,
     @"height":@(self.userHeightSlider.value),
//     @"receipt":transReceipt, // TODO: Restore receipt once amazon is willing to accept
     @"transIdentifier":transIdentifier}
           withCompletion:^(NSNumber* serverResponse)
     {
         BOOL wasComplete = [serverResponse boolValue];
         
         if (wasComplete)
         {
             UIAlertView* thankYou = [[UIAlertView alloc] initWithTitle:@"Thank You!" message:@"Your image has been recieved. We will email you once it's visible in the app" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [thankYou show];
         }
         else
         {
             UIAlertView* error = [[UIAlertView alloc] initWithTitle:@"An Error Occured" message:@"We could not process your request, please contact support@rdvsystems.com in order to proceed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [error show];
         }
         
         self.loadingView.hidden = YES;
         [self dismissViewControllerAnimated:YES completion:nil];
     }];
}

- (void) purchaseFailed
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Could not process purchase" message:@"Please try again later, make sure in-app-purchases are enabled in your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    self.loadingView.hidden = YES;
}

#pragma mark Events
- (IBAction)imageTapped:(id)sender
{
    // Create the sheet
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Choose Existing", nil];
    
    [sheet showInView:self.view];
}

- (IBAction)sliderValueChanged:(id)sender
{
    NSString* suffix = @"inch";
    
    // Check if the user uses the metric system
    if ([[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue])
    {
        suffix = @"cm";
    }
    
    self.userHeightLabel.text = [NSString stringWithFormat:@"%1.0f %@", self.userHeightSlider.value, suffix];
}

- (IBAction)submitButtonTapped:(id)sender
{
    NSString* alert = nil;
    
    // Validations:
    if (self.userImage.image == nil)
    {
        [self imageTapped:nil];
    }
    else
    {
        // Make the purchase
        [FEPurchaseManager instance].productIdentifier = [NSBundle mainBundle].infoDictionary[WOW_IAP_PERMMODEL];
        [[FEPurchaseManager instance] LoadStore];
        
        self.loadingView.hidden = NO;        
    }
    
    // Present the error
    if (alert)
    {
        UIAlertView* error = [[UIAlertView alloc] initWithTitle:@"Could not upload" message:alert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [error show];
    }
}

- (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    // Check if the user didn't cancel
    if (actionSheet.cancelButtonIndex != buttonIndex)
    {
        // Gallery
        if  (buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        // Camera
        else if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
        {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        
        // Choose image
        if (isiPad)
        {
            self.popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            [self.popover presentPopoverFromRect:self.userImage.frame inView:self.userImage.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        {
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

#pragma mark UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Check the text field
    if  (textField == self.userName)
    {
        [textField resignFirstResponder];
    }
    else
    {
        [self.userName becomeFirstResponder];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
    return YES;
}

#pragma mark UIImagePicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* image = info[UIImagePickerControllerOriginalImage];
    self.userImage.image = image;
    
    // Hide / Present bg
    if  (self.userImage.image != nil)
    {
        self.userImage.backgroundColor = [UIColor clearColor];
    }
    
    // Hide the label
    self.userImageLabel.hidden = self.userImage.image != nil;
    
    if (isiPad)
    {
        [self.popover dismissPopoverAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (isiPad)
    {
        [self.popover dismissPopoverAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)viewDidUnload
{
    [self setUserImage:nil];
    [self setUserEmail:nil];
    [self setUserGender:nil];
    [self setUserHeightSlider:nil];
    [self setUserHeightLabel:nil];
    [self setUserName:nil];
    [self setUserImageLabel:nil];
    [self setLoadingView:nil];
    [super viewDidUnload];
}
@end
