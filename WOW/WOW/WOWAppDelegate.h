//
//  WOWAppDelegate.h
//  WOW
//
//  Created by Elad Lebovitch on 7/16/13.
//  Copyright (c) 2013 RDV Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
